const { updateDoc, ITEMS } = require('./firebaseHelpers');
const utils = require('../../utils');
const config = require('../../config');

module.exports = {
    name: 'setprice',
    aliases: [],
    description: 'Set the price for an item for buy/sell.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    async execute(message, args, client, commandName) {
        const [ itemText, price ] = args;

        if (String(itemText).toUpperCase() in ITEMS) {
            try {
                const item = ITEMS[String(itemText).toUpperCase()];

                const amount = parseInt(price);
                if (isNaN(amount)) {
                    return await message.reply('That\'s not a valid number.');
                }

                await updateDoc(message, { price: amount }, 'items', item);
                const discMsg = `${message.author.username} **updated** price for \`${item}\` to price: \`${amount}\`.`;
                await utils.sendAlert(
                    message, 
                    message.author, 
                    discMsg, 
                    config.COLORS.GREEN, 
                    config.prefix + commandName + ' ' + args.join(' ')
                );
                await utils.sendStashLog(client, message, discMsg, 'red');
            } catch(err) {
                console.log('error!', err);
                await message.reply('Unable to update stash:', err.message ? err.message : err);
            }
        } else return await message.reply(`${args[0]} is not a valid item. Valid items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`);
    }
};
