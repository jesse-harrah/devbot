const firebase = require('firebase');

const config = require('../../config');

const headers = {
    kits: 'Repair Kits',
    grapes: 'Grapes',
    scrap: 'Scrap Metal',
    cash: 'Clean Money',
    dirty: 'Dirty Money',
    acetone: 'Acetone',
    plastic: 'Plastic',
    gold: 'Gold Bars',
    chip: 'Electronic',
    medkits: 'Med Kits',
    test: '~ Test Item ~',
    meals: 'Burgershot Meals',
};
const ITEMS = {
    GRAPES: 'grapes',
    KITS: 'kits',
    KIT: 'kits',
    SCRAP: 'scrap',
    DIRTY: 'dirty',
    DIRTYCASH: 'dirty',
    CLEAN: 'cash',
    CLEANCASH: 'cash',
    CASH: 'cash',
    MONEY: 'cash',
    ACETONE: 'acetone',
    PLASTIC: 'plastic',
    GOLD: 'gold',
    BARS: 'gold',
    CHIP: 'chip',
    MED: 'medkits',
    MEDKIT: 'medkits',
    MEDKITS: 'medkits',
    TEST: 'test',
    MEAL: 'meals',
    MEALS: 'meals'

};

function updateUserItems(message, doc, vals, collection, id) {
    const db = firebase.firestore();
    const collectionRef = db.collection(collection);
    const docRef = collectionRef.doc(id ? id : message.author.id);

    const data = doc.data();

    const currentItems = data.items;
    const updates = { };
    for (const item in vals) {
        if (item in currentItems) {
            updates[item] = currentItems[item] + vals[item];
        } else { 
            updates[item] = vals[item];
        }
    }

    const items = {
        ...currentItems,
        ...updates
    };
    console.log('items update', items);
    docRef.update({
        items: items,
        lastUpdated: Date.now()
    });
    return {
        updates,
        docId: message.author.id
    };
}

function updateItem(doc, collection, vals, fullSet, id) {
        const db = firebase.firestore();
        const collectionRef = db.collection(collection);
        const data = doc.data();
        const updates = { ...data, ...vals };
        updates['quantity'] = fullSet ? vals.quantity : data.quantity + (vals.quantity ? vals.quantity : 0);
        return collectionRef.doc(id).set(updates);
}

function createUser(message, docRef, vals) {
    const defaults = {};
    for (const key in headers) {
        defaults[key] = 0;
    }
    return docRef.set({
        id: message.author.id,
        items: {
            ...defaults,
            ...vals
        },
        name: message.author.username,
        niceName: message.author.username,quantity,
        discriminator: message.author.discriminator,
        createdDate: Date.now(),
        lastUpdated: Date.now()
    });
}

function createItem(message, collectionRef, id, vals, reject) {
    const defaults = {
        name: id,
        quantity: 0,
        price: 0,
        purchase: 0,
        createdDate: Date.now(),
        lastUpdated: Date.now()
    };
    return collectionRef.doc(id).set({
        ...defaults,
        ...vals
    });
}

module.exports = {
    dontLoad: true,
    ITEMS,
    headers,
    getUserByName: (name) => {
        const db = firebase.firestore();
        const collectionRef = db.collection('users');
        return collectionRef.where('name', '==', name).get();
    },
    getItemByName: (name) => {
        const db = firebase.firestore();
        const collectionRef = db.collection('items');
        return collectionRef.where('name', '==', name).get();
    },
    getDoc: (id, collection = 'users') => {
        const db = firebase.firestore();
        const collectionRef = db.collection(collection);
        const docRef = collectionRef.doc(id);
        return docRef.get();
    },
    getItems: async () => {
        const snapshot = await firebase.firestore().collection('items').get();
        return snapshot.docs.map(doc => doc.data());
    },
    getEmployees: async () => {
        const snapshot = await firebase.firestore().collection('employees').get();
        return snapshot.docs.map(doc => doc.data());
    },
    updateDoc: async (message, vals, collection = 'users', id = null, fullSet = false) => {
        return new Promise((resolve, reject) => {
            const db = firebase.firestore();
            const collectionRef = db.collection(collection);
            const docRef = collectionRef.doc(id ? id : message.author.id);

            docRef.get().then(doc => {
                if (doc.exists) {
                    if (collection === 'users') { 
                        resolve(updateUserItems(message, doc, vals, collection, id));                        
                    } else if (collection === 'items') { 
                        resolve(updateItem(doc, collection, vals, fullSet, id));
                    }
                } else {
                    if (collection === 'users') {
                        resolve(createUser(message, docRef, vals));
                    } else if (collection === 'items') {
                        createItem(message, collectionRef, id, vals, reject);
                        resolve(message.channel.send(`Item \`item\` created.`));
                    }
                }
            });     
        });
    },
    async setNiceName(id, niceName) {
        const db = firebase.firestore();
        const collectionRef = db.collection('users');
        const docRef = collectionRef.doc(id);

        const doc = docRef.get();
        console.log('exists', doc.exists);
        if (doc.exists) return await docRef.update({ niceName: niceName, lastUpdated: Date.now() });
        else return;
    },
    sendEmbed: (message, doc) => {
        console.log('doc vals', doc.data());
        const data = doc.data();
        const space =  { name: '\u200B', value: '\u200B' };

        const embed = {
            title: data.niceName ? `${data.niceName} - ${data.name}` : data.name,
            thumbnail: { url: config.LOGO },
            color: config.COLORS.GREEN,
            fields: [  ],
            timestamp: new Date(),
            footer: { 
                text: `Coded with ❤ • Last updated ${new Date(data.lastUpdated).toLocaleTimeString()}`
            }
        };

        let c = 1;
        for (const item in data.items) {
            if (item === 'test') continue;
            if (c % 2) embed.fields.push(space);
            c++;
            const amount = data.items[item];
            embed.fields.push({
                name: headers[item],
                value: amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                inline: true
            });
        }
        embed.fields.push(space);
        message.channel.send({ embeds: [ embed ] });
    },
    getSetup() {

    },
    updateSetup(message, vals) {
        const db = firebase.firestore();
        const collectionRef = db.collection('bot-settings');
        const docRef = collectionRef.doc(message.author.id);

        docRef.get().then(doc => {
            docRef.update({
                ...vals
            });
        });
    }
};
