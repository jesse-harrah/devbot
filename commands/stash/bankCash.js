const firebase = require('firebase');
const utils = require('../../utils');
const config = require('../../config');

module.exports = {
    name: 'bankcash',
    aliases: [ 'bank' ],
    description: 'Bank money in the stash.',
    usage: '<quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    // prune: true,
    async execute(message, args, client, commandName) {
        let amount = parseInt(args[0]);
        if (isNaN(amount))
            return await utils.sendAlert(message, message.author, `That's not a valid number!`, 'red');
        if (amount <= 0)
            return await utils.sendAlert(message, message.author, `\`amount\` must be greater than 0.`, 'red');

        try {
            const db = firebase.firestore();
            const collectionRef = db.collection('users');
            const userDocRef = collectionRef.doc(message.author.id);
            const userDoc = await userDocRef.get()
            const userData = await userDoc.data();

            await userDocRef.update({
                cashBanked: (userData.cashBanked || 0) + amount
            });

            const discMsg = `${userData.niceName ? userData.niceName : userData.name} banked \`${amount}\` cash. **User Total**: ${(userData.cashBanked || 0) + amount}`;
            await utils.sendAlert(message, message.author, discMsg, 'green', config.prefix + commandName + ' ' + args.join(' '));
            await utils.sendStashLog(client, message, discMsg, 'red');
        } catch (err) {
            await utils.sendAlert(message, message.author, `Unable to update stash: ${err.message ? err.message : err}`);
            console.log('err', err);
        }
    }
};
