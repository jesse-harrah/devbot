const { updateDoc, getDoc, ITEMS } = require('./firebaseHelpers');
const utils = require('../../utils');
const config = require('../../config');

module.exports = {
    name: 'add',
    aliases: [],
    description: 'Add items to the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    // prune: true,
    async execute(message, args, client, commandName) {
        const [ itemText, qty ] = args;

        if (String(itemText).toUpperCase() in ITEMS) {
            const item = ITEMS[String(itemText).toUpperCase()];

            const amount = parseInt(qty);

            if (isNaN(amount))
                return message.reply('That\'s not a valid number.');
            if (amount <= 0)
                return message.reply(`\`amount\` must be greater than 0.`);

            try {
                const userRes = await updateDoc(message, { [ item ]: amount });
                const userDoc = await getDoc(userRes.docId);
                const userData = userDoc.data();
                await updateDoc(message, { quantity: amount }, 'items', item);

                const discMsg = `${userData.niceName ? userData.niceName : userData.name} **added** \`${amount}\` x \`${item}\` to the stash.`;

                await utils.sendAlert(message, message.author, discMsg, 'green', config.prefix + commandName + ' ' + args.join(' '));
                await utils.sendStashLog(client, message, discMsg, 'green');

            } catch (err) {
                await utils.sendAlert(message, message.author, 'Unable to update stash:', err.message ? err.message : err, 'red');
            }
        } else return await utils.sendAlert(message, message.author, `\`${itemText}\` is not a valid item.\n\nValid Items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`, 'red');
    }
};
