const { updateDoc, getDoc, ITEMS } = require('./firebaseHelpers');
const utils = require('../../utils');
const config = require('../../config');

module.exports = {
    name: 'take',
    aliases: [],
    description: 'Take items from the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    // prune: true,
    async execute(message, args, client, commandName) {
        const [ itemText, qty ] = args;

        if (String(itemText).toUpperCase() in ITEMS) {
            const item = ITEMS[String(itemText).toUpperCase()];

            let amount = parseInt(qty);

            if (isNaN(amount))
                return await utils.sendAlert(message, message.author, `That's not a valid number!`, 'red');
            if (amount <= 0)
                return await utils.sendAlert(message, message.author, `\`amount\` must be greater than 0.`, 'red');

            amount = -amount;

            try {
                const userRes = await updateDoc(message, { [ item ]: amount });
                const userDoc = await getDoc(userRes.docId);
                const userData = userDoc.data();
                await updateDoc(message, { quantity: amount }, 'items', item);

                const discMsg = `${userData.niceName ? userData.niceName : userData.name} **removed** \`${-amount}\` x \`${item}\` from the stash.`;
                await utils.sendAlert(message, message.author, discMsg, 'green', config.prefix + commandName + ' ' + args.join(' '));
                await utils.sendStashLog(client, message, discMsg, 'orange');
            } catch (err) {
                await utils.sendAlert(message, message.author, `Unable to update stash: ${err.message ? err.message : err}`);
                console.log('err', err);
            }
        } else return await utils.sendAlert(message, message.author, `\`${itemText}\` is not a valid item. Valid items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`, 'red');
    }
};
