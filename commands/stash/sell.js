const { updateDoc, getItemByName, ITEMS } = require('./firebaseHelpers');
const utils = require('../../utils');
const config = require('../../config');

module.exports = {
    name: 'sell',
    aliases: [],
    description: 'Sell items from the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    // prune: true,
    async execute(message, args, client, commandName) {
        const [ itemText, qty ] = args;

        if (String(itemText).toUpperCase() in ITEMS) {
            const item = ITEMS[String(itemText).toUpperCase()];

            const amount = parseInt(qty);
            if (isNaN(amount)) {
                return message.reply('That\'s not a valid number.');
            } if (amount <= 0) {
                return message.reply(`\`amount\` must be greater than 0.`);
            }
            try {
                const itemsSnapshot = await getItemByName(item);
                let c = false;
                let data;
                itemsSnapshot.forEach(async doc => {
                    data = doc.data();
                    console.log(data);
                    if (c) return;
                    itemDoc = await updateDoc(message, { 
                        [ item ]: -amount,
                        ['cash']: data.price * amount,
                     });

                    await updateDoc(message, { quantity: -amount }, 'items', item);
                    await updateDoc(message, { quantity: data.price * amount }, 'items', 'cash');
                });
                const discMsg = `**Removed** \`${amount}\` x \`${item}\` from the stash.\n**Added** \`${data.price * amount}\` x \`money\` to the stash.`
                await utils.sendAlert(message, message.author, discMsg, 'green', config.prefix + commandName + ' ' + args.join(' '));
                await utils.sendStashLog(client, message, discMsg, 'yellow');
            } catch(err) {
                console.log('error!', err);
                await message.reply('Unable to update stash:', err.message ? err.message : err);
            }
        } else { 
            return await message.reply(`\`${itemText}\` is not a valid item. Valid items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`);
        }
    }
};
