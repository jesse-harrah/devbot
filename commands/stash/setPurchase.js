const { updateDoc, ITEMS } = require('./firebaseHelpers');
const utils = require('../../utils');
const config = require('../../config');

module.exports = {
    name: 'setpurchase',
    aliases: [],
    description: 'Add items to the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    async execute(message, args, client, commandName) {
        const [ itemText, qty ] = args;

        if (String(itemText).toUpperCase() in ITEMS) {
            try {
                const item = ITEMS[String(itemText).toUpperCase()];

                const amount = parseInt(qty);
                if (isNaN(amount)) {
                    return message.reply('That\'s not a valid number.');
                }

                await updateDoc(message, {
                    purchase: amount
                }, 'items', item);
                const discMsg = `${message.author.username} **updated** purchase price for \`${item}\` to price: \`${amount}\`.`;
                await utils.sendAlert(message, message.author, discMsg, config.COLORS.GREEN, config.prefix + commandName + ' ' + args.join(' '));               
                await utils.sendStashLog(client, message, discMsg, 'red');
            } catch(err) {
                console.log('error!', err);
                message.reply('Unable to update stash:', err.message ? err.message : err);
            }
        } else return message.reply(`\`${itemText}\` is not a valid item. Valid items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`);
    }
};
