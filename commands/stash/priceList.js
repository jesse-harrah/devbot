const { getItems, headers } = require('./firebaseHelpers');
const config = require('../../config');

module.exports = {
    name: 'pricelist',
    aliases: [],
    description: 'Check items in the stash.',
    usage: '[user]',
    args: false,
    tagUser: false,
    cooldown: 0,
    async execute(message, args, client, commandName) {
        const doc = await getItems();
        const data = doc;
        const space =  { name: '\u200B', value: '\u200B' };

        const embed = {
            title: config.BUSINESS_NAME + ' ' + config.spacer + ' `' + (config.prefix + commandName + ' ' + args.join(' ')).trim() +  '\`',
            thumbnail: { url: config.LOGO },
            fields: [  ],
            timestamp: new Date(),
            footer: { 
                text: `Coded with ❤ • Last updated ${new Date(data.lastUpdated).toLocaleTimeString()}`
            }
        };

        let c = 1;
        for (const item of data) {
            if (item.name === 'test') continue;
            if (c % 2) embed.fields.push(space);
            c++;
            const price = item.price;
            embed.fields.push({
                name: headers[item.name],
                value: `Sell Price: ${price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}\nBuy Price: ${item.purchase.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}\nStash: ${item.quantity}`,
                inline: true
            });
        }
        embed.fields.push(space);
        console.log(embed);
        await message.reply({ embeds: [ embed ] });
    }
};
