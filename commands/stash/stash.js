const { getDoc, getUserByName, sendEmbed } = require('./firebaseHelpers');
const { getUserFromMention } = require('../../utils');

module.exports = {
    name: 'stash',
    aliases: [],
    description: 'Check items in the stash.',
    usage: '[user]',
    args: false,
    tagUser: false,
    cooldown: 0,
    async execute(message, args, client) {
        if (args.length) {
            try {
                const user = await getUserFromMention(args[0], client);

                const snapshot = await getUserByName(user.username)
                let c = false;
                snapshot.forEach(doc => {
                    if (c) return;
                    sendEmbed(message, doc);
                    c = true;
                });
                if (!c) {
                    await message.channel.send(`Error finding user for name: \`${user.username}\``);
                }
            } catch(err) {
                await message.channel.send(`Error finding user for name: \`${args.join(' ')}\`` + err.message ? err.message : err);
            }
        } else {
            try {
                const doc = await getDoc(message.author.id)
                await sendEmbed(message, doc);
            } catch(err) {
                console.error(err);
            }
        }
    }
};
