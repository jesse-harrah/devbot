const { setNiceName } = require('./firebaseHelpers');

module.exports = {
    name: 'setnicename',
    aliases: [],
    description: 'Add items to the stash.',
    usage: '<niceName>',
    args: true,
    tagUser: false,
    cooldown: 0,
    execute(message, args, client) {
        console.log('test', {message, args});
        setNiceName(message.author.id, args.join(' '));
    }
};
