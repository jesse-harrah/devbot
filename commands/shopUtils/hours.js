const { google } = require('googleapis');
const { shopName } = require('../../config.json');
const { MessageEmbed } = require('discord.js');
const { spreadsheetId, sheetName } = require('../../config.json');
const QuickChart = require('quickchart-js');
const config = require('../../config');

function getType(row) {
	let type = 'O';
	if (row[1] === 'Head Mech') {
		type = 'HM';
	} else if (row[1] === 'Mechanic') {
		type = 'M'
	} else if (row[1] === 'Tow') {
		type = 'T';
	}
	return type;
}

module.exports = {
	name: 'hours',
	aliases: [],
	// dontLoad: true,

	description: 'test',
	usage: '<week number> [show average]',
	args: false,
	tagUser: false,
	cooldown: 0,
	args: true,
	prune: true,
	permissions: '',
	execute(message, args, client) {
		const sheets = google.sheets('v4');

		sheets.spreadsheets.values.get({
			auth: client.google.jwtClient,
			spreadsheetId: spreadsheetId,
			range: sheetName
		}).then(async sheet => {
			console.log('Shop Hours: ', sheet);
			let col = 0;
			let showAvg = false;
			if (args.length) {
				const week = parseInt(args[0]);
				if (isNaN(week)) {
					return message.reply('That\'s not a valid number.');
				} else if (week <= 0) {
					return message.reply('Week number is invalid, please input a number > 0 && < 30.');
				}
				col = week;
				if (args.length > 1) {
					const arg = args[1].toLowerCase();
					if (arg === 'true' || arg === 't') showAvg = true;
				}
			}

			const labels = [];
			const lastWeekHours = [];
			const averageHours = [];
			const employees = [ ];

			for (let row of sheet.data.values) {
				const emp = { };
				let name = row[0];

				let type = getType(row);
				// TODO: HARDCODED VALUES HERE!!!!
				if (name === 'Name' || name === 'Mechs' || name === 'Tow' || name === 'Total') { console.log(`Skipping ${name} row.`); continue; };
				if (!name || name === ' ' || name === undefined) { console.log(`Skipping ${name} row. 2`); continue; }
				
				let empName = row[0].split(/ +/);
				let shortName = `${empName[0]} ${empName[1][0]}. - (${type})`;

				emp.type = type;
				emp.name = shortName;
				emp.lastWeekHours = Number(row[col + 3]);
				emp.averageHours = row[row.length - 1];
				employees.push(emp);

				labels.push(shortName);
				lastWeekHours.push(row[col + 3]);
				averageHours.push(row[row.length - 1]);
			}

			const datasets = [
				{
					label: 'Hours Last Week',
					// TODO: HARDCODED VALUES HERE!!!!
					backgroundColor: 'rgba(54, 162, 235, 0.5)',
					borderColor: 'rgb(54, 162, 235)',
					borderWidth: 1,
					data: lastWeekHours
				}
			];

			showAvg && datasets.push({
				label: 'Average Hours',
				// TODO: HARDCODED VALUES HERE!!!!
				backgroundColor: 'rgba(255, 99, 132, 0.5)',
				borderColor: 'rgb(255, 99, 132)',
				borderWidth: 1,
				data: averageHours
			});

			const chart = new QuickChart();
			chart.setConfig({
				type: 'bar',
				data: {
					labels: labels,
					datasets: datasets
				},
				options: {
					legend: {
						labels: { fontColor: '#fff' }
					},
					scales: {
						xAxes: [{
							ticks: { fontColor: '#fff' }
						}],
						yAxes: [{
							ticks: {
								beginAtZero: true,
								fontColor: '#fff'
							},
							gridLines: { color: '#aaa' }
						}]
					},
					plugins: {
						backgroundImageUrl: config.BACKGROUND,
					}
				}
			});

			const url = await chart.getShortUrl();

			const embed = new MessageEmbed()
				.setTitle(config.BUSINESS_NAME)
				// TODO: HARDCODED VALUES HERE!!!!v
				.setColor('#278f42')
				.setDescription(`Employee Hours Week ${col}\n\n\n**Top Hours**\n\n`)
				.setThumbnail(config.LOGO);

			employees.sort((a, b) => (a.lastWeekHours < b.lastWeekHours) ? 1 : -1);

			for (let i = 0; i < 3; i++) {
				embed.addField(employees[i].name, employees[i].lastWeekHours + ' hrs', true);
			}

			embed
				.setImage(url)
				.setTimestamp()
				.setFooter('Coded with ❤')

			message.channel.send({ 
				embed, 
				// component: new MessageButton()
				// 	.setStyle('url').setLabel('Shop Hours')
					// .setURL(config.HOURS_SHEET_LINK) 
			});

		}).catch(error => {
			console.log('error', error);
			message.channel.send(`The API returned an error:\n\`\`\`\n${error}\n\`\`\``);
		});
	}
};