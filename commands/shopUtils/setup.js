
const { getDoc, getUserByName, sendEmbed, getSetup, updateSetup } = require('../stash/firebaseHelpers');
const { getUserFromMention } = require('../../utils');

module.exports = {
    name: 'setup',
    aliases: [],
    description: 'Check items in the stash.',
    usage: '<setting> <value>',
    args: true,
    tagUser: false,
    cooldown: 0,
    execute(message, args, client) {
        if (args.length) {
            const user = getUserFromMention(args[0], client);
            console.log('user', user);
            getUserByName(user.username).then(snapshot => {
                let c = false;
                snapshot.forEach(doc => {
                    if (c) return;
                    sendEmbed(message, doc);
                    c = true;
                });
                if (!c) {
                    message.channel.send(`Error finding user for name: \`${user.username}\``);
                }
            }).catch(error => {
                message.channel.send(`Error finding user for name: \`${args.join(' ')}\`` + error.message ? error.message : error);
            });
        } else {
            getDoc(message.author.id)
            .then(doc => {
                sendEmbed(message, doc);
            }).catch(console.error);
        }
    }
};
