/** ON MESSAGE */

const { MessageActionRow, MessageSelectMenu } = require('discord.js');
const fs = require('fs');

module.exports = {
	name: 'restartcmd',
    niceName: 'Restart Command',
    aliases: [ 'rs' ],
    // cooldown: 5,
	async execute(message, args,  client) {

        const commandMap = message.client.commands;
        const names = commandMap.map(cmd => cmd.name);
        const temp = [];
        const opts = commandMap.map(cmd => {
            if (temp.includes(cmd.name)) return;
            temp.push(cmd.name);
            return ({
                label: cmd.name,
                description: cmd.description || 'N/A',
                value: cmd.name
            });
        }).filter(a => !!a);
        console.log(commandMap);
        console.log(names);
        console.log(opts);

        const id = 'select-' + Date.now();
        // const id = 'select';

        const row = new MessageActionRow()
        .addComponents(
            new MessageSelectMenu()
                .setCustomId(id)
                .setPlaceholder('Nothing selected')
                .addOptions(opts),
        );

        await message.reply({ content: `Select a command to reload.`, components: [ row ] });

        client.on('interactionCreate', async interaction => {
            try {
                if (!interaction.isSelectMenu()) return;

                if (interaction.customId === id) {
                    console.log(interaction.values)
                    const { message } = interaction;
                    const commandName = interaction.values[0];
                    const command = message.client.commands.get(commandName)
                        || message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

                    if (!command) {
                        return message.channel.send(`There is no command with name or alias \`${commandName}\`, ${message.author}!`);
                    }

                    const commandFolders = fs.readdirSync('./commands');
                    let folderName;
                    let fileName;

                    commandFolders.forEach(folder => {
                        if (folderName) return;
                        console.log('folder', folder);
                        for (const file of fs.readdirSync(`./commands/${folder}`)) {
                            if (file.toLowerCase() === command.name + '.js') {
                                folderName = folder;
                                fileName = file;
                            }  
                        }
                    });

                    if (!folderName || !fileName) return message.channel.send(`There was an error while reloading a command \`${command.name}\``);
                    delete require.cache[require.resolve(`../${folderName}/${fileName}`)];
                    
                    try {
                        const newCommand = require(`../${folderName}/${fileName}`);
                        message.client.commands.set(newCommand.name, newCommand);
                        await interaction.update({ content: `Command \`${newCommand.name}\` was reloaded! 🔁`, components: [ row ] });
                    } catch (error) {
                        console.error(error);
                        await interaction.update({ content: `There was an error while reloading a command \`${command.name}\`:\n\`${error.message}\`\nPlease try again.`, components: [ ]});
                    }


                    // await interaction.update({ content: `${interaction.values[0]} command reloaded!`, components: [ row ], embeds: [ ] })
                }
            } catch (error) {
                console.log(error)
                interaction.message.reply(JSON.stringify(error.message))
                interaction.message.reply(JSON.stringify(error.stack.split('\n').toString(), null, 4))
            }
        });

        
    }
};