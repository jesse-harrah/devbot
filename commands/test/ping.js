/** ON MESSAGE */
// const { MessageMenuOption, MessageMenu, MessageActionRow } = require('discord-buttons');
const { MessageActionRow, MessageSelectMenu } = require('discord.js');
const firebase = require('firebase');

class SelectMenu {
    constructor(id, placeholder, options) {
        this.id = id;
        this.placeholder = placeholder;
        this.options = options;
        return this.get();
    }
    
    get() {
        new MessageActionRow()
            .addComponents(
                new MessageSelectMenu()
                    .setCustomId(this.id)
                    .setPlaceholder(this.placeholder)
                    .addOptions(this.options),
            );
    }

    
}
const row = new MessageActionRow()
        .addComponents(
            new MessageSelectMenu()
                .setCustomId('select')
                .setPlaceholder('Nothing selected')
                .addOptions([
                    {
                        label: 'Select me',
                        description: 'This is a description',
                        value: 'first_option',
                    },
                    {
                        label: 'You can select me too',
                        description: 'This is also a description',
                        value: 'second_option',
                    },
                ]),
        );

module.exports = {
	name: 'ping',
    niceName: 'Test Command',
    description: 'Test Command',
	aliases: ['test'],
	usage: '',
    cooldown: 5,
	async execute(message, args,  client) {

        /**
         * EMPLOYEE
         * 
         * created
         * updated
         * name
         * niceName?
         * discord
         * totalHours
         * weeklyHours <Array>
         * Hire Date
         * Leave Date
         * Reason
         * Notes
         * 
         */

        // some async db call to get employees
        // list employees
        // some async call to get employee data / hours / anything really


        const snapshot = await firebase.firestore().collection('items').get();
        const items = snapshot.docs.map(doc => doc.data());
        console.log(items);
        const opts = items.map(item => ({
            label: item.name,
            description: String(item.purchase),
            value: item.name
        }));

        const id = 'select-' + Date.now();

        const row = new MessageActionRow()
        .addComponents(
            new MessageSelectMenu()
                .setCustomId(id)
                .setPlaceholder('Nothing selected')
                .addOptions(opts),
        );

        client.on('interactionCreate', async interaction => {
            try {
                

                if (!interaction.isSelectMenu()) return;
                const space =  { name: '\u200B', value: '\u200B' };
                const selection = ''
                // console.log('interaction', interaction);
                if (interaction.customId === id) {
                    console.log('test')
                    const val = interaction.values[0];
                    const item = items.find(opt => opt.name === val);
                    console.log('item', item);
                    const inline = true;
                    const embed = {
                        title: item.name,
                        // thumbnail: '',
                        fields: [
                            // space,
                            { name: 'Purchase Price', value: String(item.purchase), inline },
                            { name: 'Sell Price', value: String(item.price), inline },
                            { name: 'Quantity', value: String(item.quantity), inline },
                            // space,
                        ],
                        timestamp: new Date(),
                        footer: {
                            text: `Coded with ❤ • Last updated ${new Date(item.lastUpdated).toLocaleTimeString()}`
                        }
                    };
                    console.log(embed)
                    await interaction.update({ content: `${interaction.values[0]} was selected!`, components: [ row ], embeds: [ embed ] })
                }
            } catch (error) {
                console.log(error)
                interaction.message.reply(JSON.stringify(error.message))
            }
        });

        await message.reply({ content: 'Pong!', components: [row] });
        // console.log('interactionCreate', { message, client });

        // try {
        // const opt1 = MessageMenuOption()
        //     .setLabel('Tim Short')
        //     .setDescription('Tim Short\'s twitter password.')
        //     .setEmoji('🍹')
        //     .setValue('tim');
        
        // const opt2 =  MessageMenuOption()
        //     .setLabel('Vinny Vessucci')
        //     .setDescription('Vinny Luca\'s twitter password.')
        //     .setEmoji('✌')
        //     .setValue('vinny');

        // const opt3 =  MessageMenuOption()
        //     .setLabel('Bill Peter')
        //     .setDescription('Bill Peter\'s twitter password.')
        //     .setEmoji('💩')
        //     .setValue('bill');


        // const Menu = new MessageMenu()
        //     .setID('menu1')
        //     .setPlaceholder('Choose a character.')
        //     .addOption(opt1)
        //     .addOption(opt2)
        //     .addOption(opt3)


        //     const row = new MessageActionRow()
        //         .addComponent(Menu);
    
        //     await message.channel.send('Hello World!', { components: [ row ] });
        // } catch(e) {
        //     await message.channel.send(JSON.stringify(e.message, null, 4))
        // }
        // const row = new MessageActionRow()
        //     .addComponents(
        //         new MessageSelectMenu()
        //             .setCustomId('select')
        //             .setPlaceholder('Nothing selected')
        //             .addOptions([
        //                 {
        //                     label: 'Select me',
        //                     description: 'This is a description',
        //                     value: 'first_option',
        //                 },
        //                 {
        //                     label: 'You can select me too',
        //                     description: 'This is also a description',
        //                     value: 'second_option',
        //                 },
        //             ]),
        //     );

        // await message.reply({ content: 'Pong!', components: [row] });
    }
};



// client.on('interactionCreate', async interaction => {
// 	if (!interaction.isCommand()) return;

// 	if (interaction.commandName === 'ping') {
// 		const row = new MessageActionRow()
// 			.addComponents(
// 				new MessageSelectMenu()
// 					.setCustomId('select')
// 					.setPlaceholder('Nothing selected')
// 					.addOptions([
// 						{
// 							label: 'Select me',
// 							description: 'This is a description',
// 							value: 'first_option',
// 						},
// 						{
// 							label: 'You can select me too',
// 							description: 'This is also a description',
// 							value: 'second_option',
// 						},
// 					]),
// 			);

// 		await interaction.reply({ content: 'Pong!', components: [row] });
// 	}
// });