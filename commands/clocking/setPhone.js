const firebase = require('firebase');

const config = require('../../config');
const { getUserFromMention } = require('../../utils');
const clockinUtils = require('./clockinUtils');

module.exports = {
    name: 'setphone',
    niceName: 'Set Phone',
    aliases: [ ],
    description: 'Set the phone number for an employee.',
    usage: '<user mention> <phone number>',
    args: true,
    tagUser: false,
    cooldown: 0,
    prune: true,
    async execute(message, args, client) {
        const { author } = message;
        console.log('args', args);
        try {
            const db = firebase.firestore();
            const empCollection = db.collection('employees');

            const [ user, phone ] = args;

            if (!user) {
                return clockinUtils.sendAlert(
                    message, 
                    author, 
                    `Invalid usage, please specify a user to add.`, 
                    'red',
                );
                
            }
            if (!phone) {
                return clockinUtils.sendAlert(
                    message,
                    author,
                    `Invalid usage, phone number must be supplied.`,
                    'red',
                );
            }

            const employee = await getUserFromMention(user, client);
            const employeeId = employee ? employee.id : author.id;

            const empDocRef = empCollection.doc(employeeId);
            const empDoc = await empDocRef.get(employeeId);
            
            const member = await message.guild.members.cache.get(employeeId);
            const nickname = member && member.nickname ? member.nickname : member.user.username;
            
            if (empDoc.exists) {
                await empDocRef.update({ phone });
                clockinUtils.sendAlert(
                    message,
                    employee,
                    `Employee record for ${nickname} was updated. Phone Number: \`${phone}\`.`,
                    'green',
                );
            } else {
                clockinUtils.sendAlert(
                    message,
                    employee,
                    `No employee record for ${nickname}.`,
                    'green',
                );
            }
        } catch(error) {
            clockinUtils.sendAlert(
                message,
                author,
                `An error occurred: ${error}`,
                'red',
            );
            console.error(error);
        }
    }
};