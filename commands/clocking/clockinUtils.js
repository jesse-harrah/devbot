const firebase = require('firebase');
const config = require('../../config');

module.exports = {
    dontLoad: true,
    async getDefaultEmp(message, user, position = 'Worker', refId = null, timeIn = null) { 
        const member = await message.guild.members.cache.get(user.id);
        return ({
            name: user.username,
            id: user.id,
            discriminator: user.discriminator,
            nickname: member ? member.nickname : user.username,
            avatar: user.avatar,
            active: false,
            clockinId: refId, 
            lastClockin: timeIn,
            dutyTime: 0,
            hours: [],
            position
        });
    },
    async sendAlert(message, user, description, color = 'GREEN') {
        const member = await message.guild.members.cache.get(user.id);
        return message.channel.send({
            embeds: [{
                color: config.COLORS[color.toUpperCase()],
                title: config.BUSINESS_NAME,
                author: {
                    name: member && member.nickname ? member.nickname : member.user.username,
                    icon_url: user.avatarURL(),
                },
                description
            }],
        });
    },
    clock: async (message) => {
        const db = firebase.firestore();
        const hourLogsCollection = db.collection('hour-logs');
        const empCollection = db.collection('employees');

        const { author } = message;
        const authorId = author.id;
        const timeIn = message.createdTimestamp;

        const member = await message.guild.members.cache.get(authorId);
        const nickname = member ? member.nickname : author.username;

        const empDocRef = empCollection.doc(authorId);
        const empDoc = await empDocRef.get();

        if (!empDoc.exists) {
            const ref = hourLogsCollection.doc()

            await ref.set({
                name: author.username,
                id: authorId,
                nickname,
                time: timeIn,
                type: 'clockin'
            });
            await empDocRef.set({
                name: author.username,
                phone: null,
                id: authorId,
                discriminator: author.discriminator,
                nickname,
                avatar: author.avatar,
                active: false,
                clockinId: ref.id, 
                lastClockin: timeIn,
                dutyTime: 0,
                hours: [],
                position: 'Worker'
            });
            message.channel.send({
                embeds: [{
                    color: '#6AA84F',
                    title: `${BUSINESS_NAMWE}`,
                    author: {
                        name: nickname,
                        icon_url: author.avatarURL(),
                    },
                    description: `Created employee record for ${nickname}.`
                }]
            });
        } else {
            const empData = empDoc.data();
            console.log(empData)
            if (empData.active) {
                return await message.channel.send({
                    embeds: [{
                        color: '#D92323',
                        title: `${BUSINESS_NAMWE}`,
                        author: {
                            name: nickname,
                            icon_url: author.avatarURL(),
                        },
                        description: `You're all ready on duty, try using \`>clockout\` first!`
                    }]
                });
            } else {
                const ref = hourLogsCollection.doc()
                await ref.set({
                    name: author.username,
                    id: authorId,
                    nickname,
                    time: timeIn,
                    type: 'clockin'
                });

                await empDocRef.update({
                    active: true,
                    clockinId: ref.id,
                    lastClockin: timeIn
                });
            }
        }

        message.channel.send({
            embeds: [{
                color: '#6AA84F',
                title: `${BUSINESS_NAMWE} - Clock In`,
                author: {
                    name: nickname,
                    icon_url: author.avatarURL(),
                },
                description: `${nickname} clocked on duty.`,
                footer: { text: (new Date()).toTimeString() }
            }]
        });
    }
}