const firebase = require('firebase');

const config = require('../../config');
const { getUserFromMention } = require('../../utils');
const clockinUtils = require('./clockinUtils');

module.exports = {
    name: 'addemployee',
    niceName: 'Add Employee',
    aliases: [ 'addemp' ],
    description: 'Add an employee for clockin.',
    usage: '<user mention> <rank>',
    args: false,
    tagUser: false,
    cooldown: 0,
    // prune: true,
    // dontLoad: true,
    async execute(message, args, client) {
        const { author } = message;
        console.log('args', args);
        try {
            const db = firebase.firestore();
            const empCollection = db.collection('employees');

            const [ user, rank ] = args;

            if (!user) {
                return clockinUtils.sendAlert(
                    message, 
                    author, 
                    `Invalid usage, please specify a user to add.`, 
                    'red',
                );
                
            }
            if (!rank || !config.RANKS[rank]) {
                return clockinUtils.sendAlert(
                    message,
                    author,
                    `Invalid usage, rank must be supplied.\nAcceptable values: ${Object.keys(config.RANKS).join(', ')}.`,
                    'red',
                );
            }

            const employee = await getUserFromMention(user, client);
            const employeeId = employee ? employee.id : author.id;

            const empDocRef = empCollection.doc(employeeId);
            const empDoc = await empDocRef.get(employeeId);
            
            const member = await message.guild.members.cache.get(employeeId);
            const nickname = member && member.nickname ? member.nickname : member.user.username;
            
            if (empDoc.exists) {
                const empData = empDoc.data();
                if (empData.position === config.RANKS[rank]) {
                    return clockinUtils.sendAlert(
                        message,
                        employee,
                        `${nickname} is already rank \`${config.RANKS[rank]}\`.`,
                        'red',
                    );
                }

                await empDocRef.update({ position: config.RANKS[rank] });
                await clockinUtils.sendAlert(
                    message,
                    employee,
                    `Employee record for ${nickname} was updated. New Rank: \`${config.RANKS[rank]}\`.`,
                    'green',
                );
            } else {
                const defaults = await clockinUtils.getDefaultEmp(message, employee, config.RANKS[rank]);
                await empDocRef.set(defaults);
                await clockinUtils.sendAlert(
                    message,
                    employee,
                    `Created employee record for ${nickname}. Rank: \`${config.RANKS[rank]}\``,
                    'green',
                );
            }
        } catch(error) {
            clockinUtils.sendAlert(
                message,
                author,
                `An error occurred: ${error}`,
                'red',
            );
            console.error(error);
        }
    }
};