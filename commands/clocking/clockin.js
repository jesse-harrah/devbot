const firebase = require('firebase');

const config = require('../../config');

const BUSINESS_NAMWE = 'Burgershot';

module.exports = {
    name: 'clockin',
    aliases: [],
    description: 'Clock on duty.',
    usage: '',
    args: false,
    tagUser: false,
    cooldown: 0,
    prune: true,
    // dontLoad: true,
    async execute(message, args, client) {
        const db = firebase.firestore();
        const hourLogsCollection = db.collection('hour-logs');
        const empCollection = db.collection('employees');

        const { author } = message;
        const authorId = author.id;
        const timeIn = message.createdTimestamp;

        const member = await message.guild.members.cache.get(authorId);
        const nickname = member ? member.nickname : author.username;

        const empDocRef = empCollection.doc(authorId);
        const empDoc = await empDocRef.get();

        if (!empDoc.exists) {
            return message.channel.send({
                embeds: [{
                    color: config.COLORS.RED,
                    title: `${config.BUSINESS_NAME}`,
                    author: {
                        name: nickname,
                        icon_url: author.avatarURL(),
                    },
                    description: `Employee record for ${nickname} was not found. Try using \`>addemployee <user mention> <rank>\` first.`
                }]
            });
        } else {
            const empData = empDoc.data();
            console.log(empData)
            if (empData.active) {
                return await message.channel.send({
                    embeds: [{
                        color: config.COLORS.RED,
                        title: `${config.BUSINESS_NAME}`,
                        author: {
                            name: nickname,
                            icon_url: author.avatarURL(),
                        },
                        description: `You're all ready on duty, try using \`>clockout\` first!`
                    }]
                });
            } else {
                const ref = hourLogsCollection.doc()
                await ref.set({
                    name: author.username,
                    id: authorId,
                    nickname,
                    time: timeIn,
                    type: 'clockin'
                });

                await empDocRef.update({
                    active: true,
                    clockinId: ref.id,
                    lastClockin: timeIn
                });
            }
        }

        message.channel.send({
            embeds: [{
                color: config.COLORS.GREEN,
                title: `${config.BUSINESS_NAME} - Clock In`,
                author: {
                    name: nickname,
                    icon_url: author.avatarURL(),
                },
                description: `${nickname} clocked on duty.`,
                footer: { text: (new Date()).toTimeString() }
            }]
        });
    }
};

function messageObj(message, obj) {
    return message.channel.send(`\`\`\`\n${JSON.stringify(obj, null, 4)}\n\`\`\``)
}
