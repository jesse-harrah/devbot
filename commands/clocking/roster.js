const { getEmployees, getDoc, ITEMS } = require('../stash/firebaseHelpers');
const utils = require('../../utils');
const config = require('../../config');
const moment = require('moment');


module.exports = {
    name: 'roster',
    aliases: [],
    description: 'Display employee roster.',
    usage: '',
    args: false,
    tagUser: false,
    cooldown: 0,
    async execute(message, args, client) {
        console.log('test', 'test')
        const [ tim, nagina, ...rest ] = await getEmployees('employees');
        const employees = [ nagina, tim, ...rest ];
        console.log('employees', employees);

        const space =  { name: '\u200B', value: '\u200B' };
        
        const embed = {
            title: `${config.BUSINESS_NAME} ${config.spacer} Roster`,
            thumbnail: { url: `https://i.gyazo.com/57dbe9e40c74555d7cf613e69bbc3580.png` },
            color: config.COLORS.GREEN,
            fields: [ ],
            timestamp: new Date(),
            footer: { 
                text: `Coded with ❤`
            }
        };
        let c = 1;
        employees.forEach((emp, i) => {
            // if (i && i % 3) embed.fields.push(space);
            if (c % 2) embed.fields.push(space);
            c++
            // console.log('emp', emp)
            const timeInHours = moment.duration(emp.dutyTime).asHours().toFixed(2);

            embed.fields.push({
                name: `${emp.nickname ? emp.nickname : emp.name} ${config.spacer} ${emp.position}`,
                value: `
**Phone Number**: ${!emp.phone ? 'N/A' : emp.phone}
**On Duty?**: ${emp.active ? `:green_circle:` : `:red_circle:`}
**Current week's hours**: ${timeInHours}
${emp.hours[emp.hours.length - 1] ? 
    `**Last week's hours**: \`${emp.hours[emp.hours.length -1].hours}\`` : 
    '**Last week\'s hours**: N/A'
}
                `,
                inline: true
            })
            
        });
        embed.fields.push(space);
        message.channel.send({ embeds: [ embed ] });
        // message.channel.send(JSON.stringify(embed.fields, null, 4))
    }
};
