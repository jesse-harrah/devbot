const firebase = require('firebase');
const moment = require('moment');

const config = require('../../config');
const utils = require('../../utils');


module.exports = {
    name: 'myhours',
    niceName: 'My Hours',
    aliases: [],
    description: 'Check your current hours.',
    usage: '',
    args: false,
    tagUser: false,
    cooldown: 0,
    prune: true,
    // dontLoad: true,
    async execute(message, args, client) {
        const db = firebase.firestore();
        const empCollection = db.collection('employees');

        const { author } = message;
        const authorId = author.id;

        const member = await message.guild.members.cache.get(authorId);
        const nickname = member ? member.nickname : author.username;

        const empDocRef = empCollection.doc(authorId);
        const empDoc = await empDocRef.get(authorId);

        if (empDoc.exists) {
            const docData = empDoc.data();
            const dutyTime = docData.dutyTime || 0;
            const timeInHours = moment.duration(dutyTime).asHours().toFixed(2);
            
            message.channel.send({
                embeds: [{
                    color: config.COLORS.GREEN,
                    title: `${config.BUSINESS_NAME}`,
                    author: {
                        name: nickname,
                        icon_url: author.avatarURL(),
                    },
                    description: `${nickname} been on duty for ${timeInHours} hours this week.`
                }]
            });
        } else {
            message.channel.send({
                embeds: [{
                    color: config.COLORS.RED,
                    title: `${config.BUSINESS_NAME}`,
                    author: {
                        name: nickname,
                        icon_url: author.avatarURL(),
                    },
                    description: `Employee record for ${nickname} was not found. Try using \`>addemployee <user mention> <rank>\` first.`
                }]
            });
        }

    }
};
