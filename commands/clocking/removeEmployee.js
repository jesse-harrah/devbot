const firebase = require('firebase');

const config = require('../../config');
const { getUserFromMention } = require('../../utils');
const clockinUtils = require('./clockinUtils');

module.exports = {
    name: 'removeemployee',
    niceName: 'Remove Employee',
    aliases: [ 'delemp' ],
    description: 'Remove an employee.',
    usage: '<user mention> <rank>',
    args: false,
    tagUser: false,
    cooldown: 0,
    // prune: true,
    // dontLoad: true,
    async execute(message, args, client) {
        const { author } = message;
        try {
            const db = firebase.firestore();
            const empCollection = db.collection('employees');

            const [ user ] = args;

            if (!user) {
                return clockinUtils.sendAlert(
                    message, 
                    author, 
                    `Invalid usage, please specify a user to add.`, 
                    'red',
                );
                
            }

            const employee = await getUserFromMention(user, client);
            const employeeId = employee ? employee.id : author.id;

            const empDocRef = empCollection.doc(employeeId);
            const empDoc = await empDocRef.get(employeeId);
            
            const member = await message.guild.members.cache.get(employeeId);
            const nickname = member && member.nickname ? member.nickname : member.user.username;
            
            if (empDoc.exists) {
                const empData = await empDoc.data();
                await empDocRef.delete();
                await clockinUtils.sendAlert(
                    message,
                    employee,
                    `Employee record for ${nickname} was removed. **Rank**: \`${empData.position}\`.`,
                    'red',
                );
            } else {
                await clockinUtils.sendAlert(
                    message,
                    author,
                    `${nickname} does not have an employee record.`,
                    'red',
                );
            }
        } catch(error) {
            clockinUtils.sendAlert(
                message,
                author,
                `An error occurred: ${error}`,
                'red',
            );
            console.error(error);
        }
    }
};