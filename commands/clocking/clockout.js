const firebase = require('firebase');
const moment = require('moment');

const config = require('../../config');

module.exports = {
    name: 'clockout',
    aliases: [],
    description: 'Clock off duty.',
    usage: '',
    args: false,
    tagUser: false,
    cooldown: 0,
    prune: true,
    // dontLoad: true,
    async execute(message, args, client) {
        const db = firebase.firestore();
        const hourLogsCollection = db.collection('hour-logs');
        const empCollection = db.collection('employees');

        const { author } = message;
        const authorId = author.id;
        const timeOut = message.createdTimestamp;

        const member = await message.guild.members.cache.get(authorId);
        const nickname = member ? member.nickname : author.username;

        const empDocRef = empCollection.doc(authorId);
        const empDoc = await empDocRef.get(authorId);

        let time = '';
        let dutyTime = 0;

        if (empDoc.exists) {
            const empData = empDoc.data();

            time = moment(empData.lastClockin).fromNow(true);
            dutyTime = -moment(empData.lastClockin).diff(timeOut);

            if (empData.active) {
                clockinId = empData.clockinId;
                lastClockin = empData.lastClockin;
                await empDocRef.update({ 
                    active: false,
                    clockinId: null,
                    lastClockin: null,
                    dutyTime: empData.dutyTime + dutyTime,
                });
            } else {
                return await message.channel.send({
                    embeds: [{
                        color: config.COLORS.RED,
                        title: `${config.BUSINESS_NAME}`,
                        author: {
                            name: nickname,
                            icon_url: author.avatarURL(),
                        },
                        description: `You aren't on duty, try using \`>clockin\` first!`
                    }]
                });
            }
        } else {
            return message.channel.send({
                embeds: [{
                    color: config.COLORS.RED,
                    title: `${config.BUSINESS_NAME}`,
                    author: {
                        name: nickname,
                        icon_url: author.avatarURL(),
                    },
                    description: `Employee record for ${nickname} was not found. Try using \`>addemployee <user mention> <rank>\` first.`
                }]
            });
        }

        await hourLogsCollection.doc().set({
            name: author.username,
            id: authorId,
            nickname,
            time: timeOut,
            type: 'clockout'
        });

        message.channel.send({
            embeds: [{
                color: config.COLORS.GREEN,
                title: `${config.BUSINESS_NAME} - Clock Out`,
                author: {
                    name: nickname,
                    icon_url: author.avatarURL(),
                },
                description: `${nickname} clocked off duty.
**Time on duty**: ${time}`,
                footer: { text: (new Date()).toTimeString() }
            }]
        });
    }
};

function messageObj(message, obj) {
    return message.channel.send(`\`\`\`\n${JSON.stringify(obj, null, 4)}\n\`\`\``)
}
