module.exports = {
	name: 'prune',
    aliases: [],
	description: 'Prune a channel by <amount> messages!',
    usage: '<amount>',
    args: true,
    tagUser: false,
    cooldown: 0,
    permissions: 'ADMINISTRATOR',
    execute(message, args) {
        const amount = parseInt(args[0]);

        if (isNaN(amount)) {
            return message.reply('That\'s not a valid number.');
        } else if (amount <= 0) {
            return message.reply('Amount invalid, please input a number > 0.');
        }

        message.channel.bulkDelete(amount + 1, true).then(res => {
            console.log(`Message pruned by ${amount} messages.`);
            // console.info('res:', res); // Collection of Messages that were deleted
        }).catch(err => {
            console.error(err);
            message.channel.send(`There was an error trying to prune this channel!\n${err.message || 'No error to display.'}`);
        });
    }
};