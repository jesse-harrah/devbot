const fs = require('fs');

module.exports = {
	name: 'reload',
	description: 'Reloads a command',
	args: true,
    prune: true,
    permissions: 'ADMINISTRATOR',
	execute(message, args) {
        console.log('executing reload', args)
		const commandName = args[0].toLowerCase();
		const command = message.client.commands.get(commandName)
			|| message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

		if (!command) {
			return message.channel.send(`There is no command with name or alias \`${commandName}\`, ${message.author}!`);
		}

        const commandFolders = fs.readdirSync('./commands');
        let folderName;
        let fileName;

        commandFolders.forEach(folder => {
            if (folderName) return;
            console.log('folder', folder);
            for (const file of fs.readdirSync(`./commands/${folder}`)) {
                if (file.toLowerCase() === command.name + '.js') {
                    folderName = folder;
                    fileName = file;
                }  
            }
        });

        if (!folderName || !fileName) return message.channel.send(`There was an error while reloading a command \`${command.name}\``);
        delete require.cache[require.resolve(`../${folderName}/${fileName}`)];
        
        try {
            const newCommand = require(`../${folderName}/${fileName}`);
            message.client.commands.set(newCommand.name, newCommand);
            message.channel.send(`Command \`${newCommand.name}\` was reloaded!`);
        } catch (error) {
            console.error(error);
            message.channel.send(`There was an error while reloading a command \`${command.name}\`:\n\`${error.message}\``);
        }
	},
};

// const folderName = commandFolders.find(folder => {
//     console.log('folder', folder, command.name + '.js',  fs.readdirSync(`./commands/${folder}`)[0], fs.readdirSync(`./commands/${folder}`)[0] === command.name + '.js');
//     fs.readdirSync(`./commands/${folder}`).includes(`${command.name}.js`)
// });

// if (folder === 'youtube') console.log('test', { file, name: commandName.name + '.js' });
