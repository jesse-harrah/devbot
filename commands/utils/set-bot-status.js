const { setBotStatus } = require('../../utils');

module.exports = {
	name: 'setbotstatus',
    aliases: [ 'botstatus' ],
	description: 'Set the status for the bot.',
    usage: '<statusMessage>',
    args: true,
    tagUser: false,
    cooldown: 5,
    // prune: true,
    // permissions: '', // 'ADMINISTRATOR',
    execute(message, args, client) {
        const VALID_STATUSES = [
            'PLAYING', 'STREAMING',
            'LISTENING', 'WATCHING',
            'COMPETING'
        ];
        const [ first, ...rest ] = args;
        if (VALID_STATUSES.includes(first.toUpperCase())) {
            const type = String(args.shift()).toUpperCase() || 'PLAYING';
            let msg;
            msg = args.length ? msg = args.join(` `) : ` `;
            setBotStatus(client, msg, type);
            message.reply(`successfully set bot status to: ${type} ${msg}`);
        } else {
            let msg;
            msg = args.length ? msg = args.join(` `) : ` `;
            setBotStatus(client, msg);
            message.reply(`successfully set bot status to: ${msg}`);
        }
    }
};