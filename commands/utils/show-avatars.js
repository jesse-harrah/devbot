module.exports = {
	name: 'showavatar',
    aliases: [ 'avatar', 'icon' ],
	description: 'Show mentioned users in a message.',
    usage: '<message>',
    args: true,
    tagUser: false,
    execute(message, args) {
        const taggedUsers = message.mentions.users;
        console.log('test', 'test')
        const avatarList = taggedUsers.map(user => {
            return `${user.avatarURL({ format: 'png', dynamic: true})}`
        });
        console.log('avatarlist', avatarList)
        message.channel.send({ content: avatarList.join('\n') });
    }
};