const { updateDoc, getItemByName, ITEMS } = require('../stash/firebaseHelpers');

module.exports = {
    name: 'makemeals',
    aliases: [],
    description: 'Make meals.',
    usage: '[amount]',
    args: false,
    tagUser: false,
    cooldown: 0,
    prune: true,
    // dontLoad: true,
    async execute(message, args, client) {
        console.log('test', 'test')
        const amount = parseInt(args[0]);
        if (isNaN(amount)) {
            return message.reply('That\'s not a valid number.');
        } if (amount <= 0) {
            return message.reply(`\`amount\` must be greater than 0.`);
        }

        try {
            const itemsSnapshot = await getItemByName(item);
            let c = false;
            let data;
            itemsSnapshot.forEach(async doc => {
                data = doc.data();
                console.log(data);
                if (c) return;
                itemDoc = await updateDoc(message, { 
                    [ 'meat' ]: -amount * 2,
                    ['testmeals']: +data.testmeals + 1,
                 });

                let stashRes = await updateDoc(message, { quantity: -amount }, 'items', 'test');
                let stashRes2 = await updateDoc(message, { quantity: data.price * amount }, 'items', 'cash');
            });

            message.channel.send(`Removed \`${amount} x ${item}\` from the stash.
Added \`${data.price * amount} x ${'meals'}\` to the stash.`);
        } catch(err) {
            console.log('error!', err);
            message.reply('Unable to update stash:', err.message ? err.message : err);
        }
    


    }
};
