const APPLICATIONS_WEBHOOK_ID = '825412469735948348';

const APPLICATIONS_CHANNEL_ID = '825412645502189618';
const DENIED_CHANNEL_ID = '896767554561917000';
const ACCEPTED_CHANNEL_ID = '896767896154423366';

const LOG_CHANNEL_ID = '810822823756562493';
const STASH_LOG_CHANNEL_ID = '898681063650512928';

//%F0%9F%9F%A2 // %F0%9F%9F%A0 //%F0%9F%94%B4
const EMOJI_GREEN_CIRCLE = '%F0%9F%9F%A2';
const EMOJI_ORANGE_CIRCLE = '%F0%9F%9F%A0';
const EMOJI_RED_CIRCLE = '%F0%9F%94%B4';


const EMOJI_GREEN_CIRCLE_ID = '🟢';
const EMOJI_ORANGE_CIRCLE_ID = '🟠';
const EMOJI_RED_CIRCLE_ID = '🔴';

const EMOJI_X_ID = '%E2%9D%8C';
const EMOJI_CHECK_ID = '%E2%9C%85';

const EMOJI_X = '✅';
const EMOJI_CHECK = '❌';

module.exports = {
	// Bot token
    token: 'ODUyMzc3NjIyMTUxMDM2OTQ4.YMF8hQ.8zObe0hNF6nfqlZ_IINpJpi69Zs',

    LOGO: `https://i.gyazo.com/57dbe9e40c74555d7cf613e69bbc3580.png`,
    BACKGROUND: `https://wallpaper-mania.com/wp-content/uploads/2018/09/High_resolution_wallpaper_background_ID_77700372120-768x480.jpg`,

    HOURS_SHEET_LINK: 'https://docs.google.com/spreadsheets/d/1xQ5miB2CMB-4TlLzLpaEJ0rY-YpIgkLzvLRp2IUaZSc/edit#gid=844948593',

    APPLICATIONS_WEBHOOK_ID,
    APPLICATIONS_CHANNEL_ID,
    DENIED_CHANNEL_ID,
    ACCEPTED_CHANNEL_ID,

    LOG_CHANNEL_ID,
    STASH_LOG_CHANNEL_ID,

    EMOJI_GREEN_CIRCLE,
    EMOJI_ORANGE_CIRCLE,
    EMOJI_RED_CIRCLE,

    EMOJI_GREEN_CIRCLE_ID,
    EMOJI_ORANGE_CIRCLE_ID,
    EMOJI_RED_CIRCLE_ID,

    EMOJI_X,
    EMOJI_CHECK,

    EMOJI_X_ID,
    EMOJI_CHECK_ID,
    
    prefix: '>',
    spacer: '•',

    clientId: "123456789012345678", // CHANGE ME
	guildId: "876543210987654321",  // CHANGE ME

	BUSINESS_NAME: 'Sinclair T&R',
	shopName: 'Sinclair T&R',

    // Hours Spreadsheet
	spreadsheetId: '1xQ5miB2CMB-4TlLzLpaEJ0rY-YpIgkLzvLRp2IUaZSc', 
	sheetName: 'Hours!A1:AI20',

	firebaseKeys: {
		apiKey: 'AIzaSyCVJiUeXFVMJAWu9G20PE4hCe1koZsDPwA',
		authDomain: 'onesas-1616806009621.firebaseapp.com',
		projectId: 'onesas-1616806009621',
		storageBucket: 'onesas-1616806009621.appspot.com',
		messagingSenderId: '63103234754',
		appId: '1:63103234754:web:b59e3e5a19597b3a5b815b',
		measurementId: 'G-FKT3SXNJP2'
	},

    COLORS: {
        RED: '#D92323',
        GREEN: '#6AA84F',
        BLUE: '',
        ORANGE: '#e69138',
        YELLOW: '#fed348',
    },

    // Clockin
    RANKS: {
        owner: 'Owner',
        headmech: 'Head Mechanic',
        mech: 'Mechanic',
        tow: 'Tow',
    }

    // Application Reactions


}