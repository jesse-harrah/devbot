const fs = require('fs');
const Discord = require('discord.js');
const { Message } = require('discord.js');
const config = require('./config');
const { description } = require('./commands/clocking/myhours');
// const config = require('../../config');

async function getUserFromMention(mention, client) {
	if (!mention) return;
	if (mention.startsWith('<@') && mention.endsWith('>')) {
		mention = mention.slice(2, -1);
		if (mention.startsWith('!')) {
			mention = mention.slice(1);
		}
		return client.users.cache.get(mention);
	}
}

function banUser(user, guild) {
	guild.members.ban(user);
}

function unbanUser(id, guild) {
	guild.members.unban(id)
}

function reactionCollector(callback, error) {
	const filter = (reaction, user) => reaction.emoji.name === '👍' && user.id === Message.author.id;

	Message.awaitReactions(filter, { max: 4, time: 60000, errors: [ 'time' ]})
		.then(collected => {
			console.log({ size: collected.size, collected });
			if (callback) callback(collected);
		}).catch(collected => {
			if (error) {
				error(collected);
			} else console.log(`After a minute, only ${collected.size} out of 4 reacted.`)
		});
}

function setBotStatus(client, status, type = 'PLAYING') {
    console.log('test', {client, status, type})
    if (status) client.user.setActivity(status, { type: type });
}

function setupEvents(client) {
    const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'));
    for (const file of eventFiles) {
        const event = require(`./events/${file}`);
        if (event.once) {
            client.once(event.name, (...args) => event.execute(...args, client));
        } else {
            console.log(`Setting up ${event.niceName || event.name} for 'on'.`);
            client.on(event.name, (...args) => event.execute(...args, client));
        }
    }
}

function setupCommands(client) {
    client.commands = new Discord.Collection();
    client.cooldowns = new Discord.Collection();

    const commandFolders = fs.readdirSync(`./commands`);
    // Add commands to collection
    for (const folder of commandFolders) {
        const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith(`.js`));
        for (const file of commandFiles) {
            const command = require(`./commands/${folder}/${file}`);
            if (command.dontLoad) continue;
            console.info(`Adding command ~~~~~~~ '${command.name}'.`);
            client.commands.set(command.name, command);
            if (command.aliases) {
                for (const alias of command.aliases) {
                    console.info(`Adding alias ~~~~~~~~~ '${alias}'.`);
                    client.commands.set(alias, command);  
                }
            }
        }
    }
}

function truncateStr(str, maxLength) {
    return str.substr(0, maxLength);
}

function updateStashMsg(strings, ...args) {
    `${userData.niceName ? userData.niceName : userData.name}
     ${amount >= 0 ? 'added' : 'removed'} \`${amount} x ${item}\` to the stash.`
    return 'hello';
}

function messageObj(message, obj) {
    return message.channel.send(`\`\`\`\n${JSON.stringify(obj, null, 4)}\n\`\`\``)
}

const sendAlert = async (message, user, description, color = 'GREEN', title = '') => {
    const member = await message.guild.members.cache.get(user.id);
    const msg = await message.reply({
        embeds: [{
            color: config.COLORS[color.toUpperCase()],
            title: `${config.BUSINESS_NAME}${title ? ` ${config.spacer} \`` + title + '\`' : ''}`,
            author: {
                name: member && member.nickname ? member.nickname : member.user.username,
                icon_url: user.avatarURL(),
            },
            description
        }]
    });
    return msg;
}

const sendStashLog = async (client, message, description, color = 'GREEN') => {
    const { author } = message;
    const member = await message.guild.members.cache.get(author.id);
    const embed = {
        color: config.COLORS[color.toUpperCase()],
        author: {
            name: member && member.nickname ? member.nickname : member.user.username,
            icon_url: author.avatarURL(),
        },
        fields: [
            {
                name: 'User',
                value: `<@${author.id}>`,
                inline: true
            },
            {
                name: 'Channel',
                value: `<#${message.channel.id}>`,
                inline: true
            },
        ],
        description,
    };
    // message.channel.send(JSON.stringify(embed, null, 4))
    const msg = await client.channels.cache.get(config.STASH_LOG_CHANNEL_ID).send({ 
        embeds: [ embed ], 
    });
}

const sendLog = async (client, message, description, color = 'GREEN') => {
    try {
        const { author } = message;
        const member = await message.guild ? message.guild.members.cache.get(author.id) : { user: { username: message.author.username } };
        const embed = {
            color: config.COLORS[color.toUpperCase()],
            // title: 'Bot Command',
            author: {
                name: member && member.nickname ? member.nickname : member.user.username + ' Used a Bot Command!',
                icon_url: author.avatarURL(),
            },
            fields: [
                {
                    name: 'User',
                    value: `<@${author.id}>`,
                    inline: true
                },
                {
                    name: 'Channel',
                    value: `<#${message.channel.id}>`,
                    inline: true
                },
                {
                    name: 'Command',
                    value: `\`${message.content}\``,
                    inline: true
                },

            ],
            description,
        };
        // message.channel.send(JSON.stringify(embed, null, 4))
        const msg = await client.channels.cache.get(config.LOG_CHANNEL_ID).send({ 
            embeds: [ embed ], 
        });
    } catch(err) {
        message.reply(`An error occured: ${err.message ? err.message : err}`)
    }
}

module.exports = {
    config,
    getUserFromMention,
	banUser,
	unbanUser,
	reactionCollector,
	setBotStatus,
	setupEvents,
	setupCommands,
    truncateStr,
    messageObj,
    sendAlert,
    sendLog,
    sendStashLog
};
