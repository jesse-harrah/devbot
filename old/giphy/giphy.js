const { giphyAPIKey } = require('../../config.json');
const axios = require('axios');

module.exports = {
	name: 'giphy',
    aliases: [ 'gif' ],
	description: 'Start a quiz!',
    usage: '[search terms]',
    args: false,
    tagUser: false,
    cooldown: 0,
    execute(message, args, client) {
        const limit = 5;
        const searchTerms = args.join('%20'); // Join words with URLEncoded space
        const url = `https://api.giphy.com/v1/gifs/search?api_key=${ giphyAPIKey }&q=${searchTerms}&limit=${limit}&offset=0&lang=en`;
        const randURL = `https://api.giphy.com/v1/gifs/random?api_key=p7EUf5FD5oJ7QMqy1k7MUXA6OYXpedP6`;

        console.log('searchTerms', { searchTerms, url });
        
        if (args.length) {
            axios.get(url).then(res => {
                // console.log(res);
                const resultURLS = [ ];
                for (const gif of res.data.data) {
                    gif.url && resultURLS.push(gif.url);
                }
                console.log('resultURLS', resultURLS);
                message.channel.send(`**(${resultURLS.length}) results for:** ${args.join(' ')}\n${resultURLS.join(' ')}`);
            }).catch(error => console.error(error))
        } else {
            axios.get(randURL).then(res => {
                // console.log(res);
                message.channel.send('**Random!**\n' + res.data.data.url);
            }).catch(error => console.error(error))
        }
    }
};