module.exports = {
	name: 'sweep',
    aliases: [],
	description: 'Sweep a channel and delete messages older than <amount> seconds!',
    usage: '<amount>',
    args: true,
    tagUser: false,
    cooldown: 5,
    permissions: 'ADMINISTRATOR',
    execute(message, args, client) {
        console.log('sweep', message)
        const amount = parseInt(args[0]);

        if (isNaN(amount)) {
            return message.reply('That\'s not a valid number.');
        } else if (amount <= 0) {
            return message.reply('Amount invalid, please input a number > 0.');
        }

        client.sweepMessages(amount);
        console.log(`Successfully removed messages older than ${amount} seconds from ${message.channel.name}`);
    }
};