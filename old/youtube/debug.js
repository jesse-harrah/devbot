const musicBot = require('./musicBot');

module.exports = {
	name: 'debug',
    aliases: [ ],
	description: 'Youtube Stats.',
    usage: '',
    async execute(message, args) {
        try {
            let serverQueue = await musicBot.queue.get(message.guild.id);
            const embed = {
                title: 'YouTube Stats',
                fields: [ { name: '\u200b', value: '\u200b' } ],
            };
            console.log('serverQueue', serverQueue);
            // console.log('serverQueue.songs'. serverQueue.songs);
            for (const key in serverQueue) {
                const val = serverQueue[key];
                console.log('PUSHING FIELD', key);
                console.log('val', val);
                if (key === 'songs') { continue;
                    let str = '';
                    val.forEach(v => {
                        str += v.title + ' - `' + v.id + '`\n'
                    });
                    embed.fields.push({
                        name: key,
                        value: String(str)
                    });
                } else if (key === 'connection' || key === 'original') {
                    continue;
                } else {
                    embed.fields.push({
                        name: key,
                        value: String(val)
                    });
                }
            }
            message.channel.send({ embed });
        } catch(err) {
            console.error(err);
            message.channel.send(`Something went wrong while trying to loop the queue: ${err.message}`);
        }
    }
};