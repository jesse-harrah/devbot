const musicBot = require('./musicBot');

module.exports = {
	name: 'skip',
    aliases: [ ],
	description: 'Skip current song.',
    usage: '',
    tagUser: false,
    async execute(message, args) {
        try {
            const id = parseInt(args[0]);
            let serverQueue = await musicBot.queue.get(message.guild.id);

            if (serverQueue.currentlyPlaying) {
                const song = serverQueue.songs.shift();
                message.channel.send(`Skipped song: \`${song.title}\`.`);
                setTimeout(() => musicBot.play(message.guild, serverQueue.songs[0]), 1000);
            }
        } catch(err) {
            message.channel.send(`Something went wrong while trying to loop the queue: ${err.message}`);
        }
    }
};