const { google } = require('googleapis');
const ytdl = require('ytdl-core');

module.exports = {
    dontLoad: true,
    ytVideoURL: `https://www.youtube.com/watch?v=`,
    queue: new Map(),
    constructQueue(message, voiceChannel) {
        return new Promise((resolve, reject) => {
            console.log('constructing')
            const queueContruct = {
                textChannel: message.channel,
                voiceChannel: voiceChannel,
                connection: null,
                original: [],
                songs: [],
                volume: 5,
                loop: false,
                playing: false,
                paused: false,
                id: 1,
                currentlyPlaying: null
            };
            this.queue.set(message.guild.id, queueContruct);
            console.log('queue constructed');
            resolve(this.queue.get(message.guild.id));
        });
    },
    async addSongs(message, videoIds, serverQueue) {
        return new Promise((resolve, reject) => {
            console.log('videoIDS!', videoIds)
            Promise.allSettled(videoIds.map((ids) => ytdl.getBasicInfo(ids))).then(songsData => {
                songsData.forEach(songInfo => {
                    console.log('songinfo?', songInfo);
                    console.log('videoDetails', songInfo.value.videoDetails);
                    // console.log('serverQueue', serverQueue);
                    // console.log('songInfo.author.thumbnails[0]', songInfo.videoDetails.thumbnails[0].url)
                    const song = {
                        id: serverQueue.id,
                        title: songInfo.value.videoDetails.title,
                        url: songInfo.value.videoDetails.video_url,
                        time: this.convertSeconds(songInfo.value.videoDetails.lengthSeconds, 'minutes'),
                        // thumbnail: songInfo.videoDetails.thumbnails[0].url,
                    };
                    console.log('song', song);
                    console.log('song.id', song.id);
                    message.channel.send({embed: { title: 'Added song to queue', description: '`' + song.title + '`', fields: [ { name: 'ID: `' + song.id + '`', value: '\u200b' }] }});
                    serverQueue.songs.push(song);
                    serverQueue.original.push(song);
                    console.log('serverQueue.songs', serverQueue.songs);
                    serverQueue.id++;
                });
            }).then(resolve).catch(reject);
            // resolve(result);
        });
    },
    play(message, song) {
        const guild = message.guild;
        console.log('playing', song);
        console.log('message', message);
        console.log('-----------------------------------------------------');
        console.log('guild', guild);
        const serverQueue = this.queue.get(guild.id);
        if (!song) {
            if (serverQueue.loop) {
                serverQueue.songs = [ ...serverQueue.original ];
                if (serverQueue.songs.length) {
                    song = serverQueue.songs[0];
                    if (!serverQueue.playing) serverQueue.playing = true;
                }
            } else {
                serverQueue.voiceChannel.leave();
                serverQueue.playing = false;
                this.queue.delete(guild.id);
                return;
            }
            if (!song || song === undefined) {
                serverQueue.voiceChannel.leave();
                serverQueue.delete(guild.id);
                return;
            }
        } else {
            if (serverQueue && !serverQueue.playing) serverQueue.playing = false; 
        }
        serverQueue.currentlyPlaying = song.id;
        const embed = {
            title: 'Started Playing',
            url: song.url,
            description: `\`${song.title}\`\n${song.url}`,
            thumbnail: song.thumbnail
        };
        serverQueue.connection
            .play(ytdl(song.url))
            .on('finish', () => {
                serverQueue.songs.shift();
                // loopQueue.songs.shift();
                // console.log('finish', serverQueue);
                this.play(message, serverQueue.songs[0]);
                if (!serverQueue.loop) serverQueue.textChannel.send({ embed });
            })
            .on('error', err => console.error(err))
            .on('end', () => {
                setTimeout(() => {
                    message.channel.send(`Song ended so I'm leaving.`);
                    serverQueue.voiceChannel.leave();
                }, 5000);
            }) // Log any stream errors that might occurr.
            .on('finsih', (e) => {
                console.log('finsih');
            })
            .on('unpipe', (e) => {
                console.log('unpipe');
            })
            .on('close', (e) => {
                console.log('close');
                // setTimeout(() => {
                //     message.channel.send(`close so I'm leaving.`);
                //     message.channel.send(`DEBUG: Queue length when closing: ${serverQueue.songs.length}`)
                //     serverQueue.voiceChannel.leave();
                // }, 5000);
            });
    },
    skip(message, serverQueue) {
        if (!message.member.voice.channel)
            return message.channel.send(
            "You have to be in a voice channel to stop the music!"
            );
        if (!serverQueue)
            return message.channel.send("There is no song that I could skip!");
        serverQueue.connection.dispatcher.end();
    },
    stop(message, serverQueue) {
        if (!message.member.voice.channel)
          return message.channel.send(
            "You have to be in a voice channel to stop the music!"
          );
          
        if (!serverQueue)
          return message.channel.send("There is no song that I could stop!");
          
        serverQueue.songs = [];
        serverQueue.connection.dispatcher.end();
    },
    searchYT(auth, type, query) {
        console.log('query', query);

        return new Promise((resolve, reject) => {
            const service = google.youtube('v3');
            if (type === 'song') {
                service.search.list({
                    auth: auth,
                    part: 'snippet',
                    order: 'relevance',
                    q: query,
                    type: 'video'
                    // forUsername: 'GoogleDevelopers'
                }).then(async res => { 
                    // console.log('response', res);
                    const videos = res.data.items;
                    if (videos[0]) {
                        if (videos[0].id.videoId) {
                            console.log('video id', videos[0].id.videoId);
                            const videoId = videos[0].id.videoId;
                            resolve([ `${videoId}` ]);
                        }
                    }
                }).catch(reject);
            } else if (type === 'playlist') {
                service.playlistItems.list({
                    auth: auth,
                    part: 'snippet',
                    order: 'relevance',
                    playlistId: query,
                    type: 'video',
                    maxResults: 50, // default: 5
                    // forUsername: 'GoogleDevelopers'
                }).then(async res => { 
                    console.log('response', res);
                    const videos = res.data.items;
                    let videoIds = [];
                    videos.forEach(video => {
                        console.log('playlist video:' , video);
                        videoIds.push(video.snippet.resourceId.videoId)
                    });
                    resolve(videoIds);
                }).catch(reject);
            } else if (type === 'watch') {
                resolve([ `${query}` ]);
            } else {
                console.error('Invalid search type specified.');
            }
        });
    },
    convertSeconds(s, to) {
        if (to === 'minutes') {
            const minutes = Math.floor(s / 60);
            let seconds = s - (minutes * 60);
            if (seconds.length === 1) seconds = `0${seconds}`;
            return `${minutes}:${seconds}`;
        }
    }
};
