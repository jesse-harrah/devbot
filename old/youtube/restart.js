module.exports = {
	name: 'restart',
	description: 'Stops the bot server, will restart automatically with PM2.',
    execute() {
        process.exit();
    }
};