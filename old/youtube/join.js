const { queue } = require('./musicBot');
const musicBot = require('./musicBot');
const URL = require('url').URL;


function embedDebug(message, obj, name) {
    const embed = {
        title: `Debug object${name ? `: ${name}` : ``}`,
        fields: [ { name: '\u200b', value: '\u200b' } ],
    };

    for (const key in obj) {
        const val = obj[key];                 
        embed.fields.push({
            name: key,
            value: String(`\`${val ? val : ` `}\``)
        });
    }

    message.channel.send({ embed });
}

function sendJSON(message, thing, name = 'JSON: ') {
    message.reply(`${name}\`\`\`json
${JSON.stringify(thing)}
\`\`\``);
}

module.exports = {
    name: 'join',
    aliases: ['play', 'yt'],
    description: 'Join a voice channel and play a youtube video.',
    usage: '<youtube-link>',
    args: true,
    shouldLoad() {
        try {
            const resolved = require.resolve('@discordjs/opus');
            console.log('Found audio lib:', resolved);
            return true;
        } catch (err) {
            console.error('> "@discordjs/opus" is not installed.');
            try {
                const resolved = require.resolve('opusscript');
                console.log('Found audio lib:', resolved);
                return true;
            } catch (err2) {
                console.error('> "opusscript" is not installed.');
                return false;
            }
        }
    },
    isURL(str) {
        try {
            return new URL(str);
        } catch (err) {
            return null;
        }
    },
    async execute(message, args, client) {

        if (!this.shouldLoad()) return;

        const voiceChannel = message.member.voice.channel;
        if (!voiceChannel) return message.reply('You must be in a voice channel first.');
        if (!voiceChannel.joinable) {
            message.reply('Cannot join your current voice channel.');
            return;
        } else { console.log('Voice channel joinable!'); }

        try {
            const connection = await voiceChannel.join();

            message.channel.send(`:thumbsup: Joined \`${message.guild.name}\` and bound to \`#${message.channel.name}\``);
            let serverQueue = await musicBot.queue.get(message.guild.id);

            if (!serverQueue) {
                serverQueue = await musicBot.constructQueue(message, voiceChannel);
                serverQueue.connection = connection;
            }
            const url = this.isURL(args[0]);
            if (args.length && url) {
                console.log('url', url);
                embedDebug(message, url, 'url');
                if (url.pathname.slice(1) === 'playlist') {
                    console.log(url.searchParams.get('list'))
                    message.channel.send(`Playlist Id: ${url.searchParams.get('list')}`);
                    musicBot.searchYT(client.google.auth, 'playlist', url.searchParams.get('list'))
                        .then(res => {
                            sendJSON(message, res);
                            musicBot.addSongs(message, res, serverQueue).then((res) => {
                                serverQueue && !serverQueue.playing && musicBot.play(message, serverQueue.songs[0]);
                            });
                        })
                        .catch(err => sendJSON(message, err));
                } else if (url.pathname.slice(1) === 'watch') {
                    console.log('videoID', url.searchParams.get('v'));
                    message.channel.send(`Video Id: ${url.searchParams.get('v')}`);
                    musicBot.searchYT(client.google.auth, 'watch', url.searchParams.get('v'))
                        .then(res => {
                            sendJSON(message, res);
                            musicBot.addSongs(message, res, serverQueue).then((res) => {
                                console.log('serverQueue', serverQueue);
                                serverQueue && !serverQueue.playing && musicBot.play(message, serverQueue.songs[0]);
                            });
                        });
                }
            } else if (args.length && !url) {
                message.channel.send(`:mag_right: Searching YT for: \`${args.join(' ')}\``);
                // message.channel.send(`:youtube: Searching  ${args.join(' ')}`);
                musicBot.searchYT(client.google.auth, 'song', args.join('+'))
                    .then(async videoIds => {
                        console.log('videoIds', videoIds);
                        musicBot.addSongs(message, videoIds, serverQueue).then(() => {
                            !serverQueue.playing && musicBot.play(message.guild, serverQueue.songs[0]);
                        });   
                    });
            }

        } catch (err) {
            queue.delete(message.guild.id);
            voiceChannel.leave();
            message.channel.send(`Something went wrong while playing song: ${err.message ? err.message : err}`);
            console.error(err);
        }
    }
};
