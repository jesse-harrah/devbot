const musicBot = require('./musicBot');

module.exports = {
	name: 'np',
    aliases: [ ],
	description: 'Now Playing.',
    usage: '',
    tagUser: false,
    async execute(message, args) {
        try {
            let serverQueue = await musicBot.queue.get(message.guild.id);
            if (!serverQueue) { return message.channel.send(`No queue for this channel currently :(.`); }

            let id = serverQueue.currentlyPlaying;
            const index = serverQueue.songs.findIndex(e => e.id === id);
            console.log('index', index);
            if (index >= 0) {
                const song = serverQueue.songs[index];  
                message.channel.send(`Now playing: \`${song.title}\`.`);
            } else {
                message.channel.send(`Invalid id specified: ${id}.`);
            }
        } catch(err) {
            message.channel.send(`Something went wrong while trying to loop the queue: ${err.message}`);
        }
    }
};