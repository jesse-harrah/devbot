const musicBot = require('./musicBot');

module.exports = {
	name: 'loop',
    aliases: [ ],
	description: 'Loop queue.',
    usage: '',
    tagUser: false,
    async execute(message, args) {
        try {
            if (!message.member.voice.channel)
            return message.channel.send(
                `You have to be in a voice channel to stop the music!`
            );
            let serverQueue = await musicBot.queue.get(message.guild.id);
            if (!serverQueue) { return message.channel.send(`No queue for this channel currently :(.`); }
    
            serverQueue.loop = !serverQueue.loop;
            message.channel.send(`Queue loop ${serverQueue.loop ? 'enabled' : 'disabled'}`);
        } catch(err) {
            message.channel.send(`Something went wrong while trying to loop the queue: ${err.message}`);
        }
    }
};