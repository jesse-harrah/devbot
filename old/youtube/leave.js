const musicBot = require('./musicBot');

module.exports = {
	name: 'leave',
    aliases: [ 'dc' ],
	description: 'disconnect',
    usage: '',
    tagUser: false,
    async execute(message, args) {
        const voiceChannel = message.member.voice.channel;
        musicBot.queue.delete(message.guild.id);
        voiceChannel.leave();
        message.channel.send(`DevBot left ${message.channel.name}`)
    }
};