const musicBot = require('./musicBot');

module.exports = {
	name: 'remove',
    aliases: [ ],
	description: 'Remove song from queue.',
    usage: '<song id>',
    args: true,
    tagUser: false,
    async execute(message, args) {
        try {
            const id = parseInt(args[0]);
            let serverQueue = await musicBot.queue.get(message.guild.id);

            if (isNaN(id)) {
                return message.reply('That\'s not a valid number.');
            } else if (id <= 0) {
                return message.reply('ID invalid, please input a number > 0.');
            }
            
            if (!serverQueue) { return message.channel.send(`No queue for this channel currently :(.`); }

            const index = serverQueue.songs.findIndex(e => e.id === id);
            console.log('index', index);
            if (index >= 0) {
                const song = serverQueue.songs[index]; 
                serverQueue.songs.splice(index, 1);
                console.log(index, song);         
                message.channel.send(`Removed song ${song.title}.`);
                if (id === serverQueue.currentlyPlaying) {
                    musicBot.play(message.guild, serverQueue.songs[0]);
                }
            } else {
                message.channel.send(`Invalid id specified: ${id}.`);
            }
        } catch(err) {
            message.channel.send(`Something went wrong while trying to loop the queue: ${err.message}`);
        }
    }
};