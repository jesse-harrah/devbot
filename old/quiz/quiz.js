const quiz = require('./questions.json');
const item = quiz[Math.floor(Math.random() * quiz.length)];
const filter = res => item.answers.some(ans => ans.toLocaleLowerCase() === res.content.toLocaleLowerCase());

module.exports = {
	name: 'quiz',
    aliases: [ 'test-quiz' ],
	description: 'Start a quiz!',
    usage: '',
    args: false,
    tagUser: false,
    cooldown: 5,
    execute(message, args, client) {
       message.channel.send(item.question).then(_ => {
            message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: [ 'time' ]})
                .then(collected => message.channel
                    .send(`${collected.first().author} got the correct answer!`))
                    .catch(_ => message.channel.send('Looks like nobody got the answer.'));
       })
    }
};