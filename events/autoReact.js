/**
 * Event based on message send, if Application Webhook then we react to the message.
 */

const REACTIONS = [
    { name: 'green_circle', id: '897029279643209728', icon: '🟢' }, 
    { name: 'orange_circle', id: '897029777687453726', icon: '🟠' },
    { name: 'red_circle', id: '897029777687453726', icon: '🔴' },
    // { name: 'white_check_mark', id: '897029777687453726', icon: '✅' },
    // { name: 'x', id: '897029777687453726', icon: '❌' },
];

const WEBHOOK_ID = '825412469735948348';

/** COMMAND HANDLING */
module.exports = {
	name: 'messageCreate',
    niceName: 'Auto-React',
	async execute(message, client) {
        const { webhookId } = message;
        // console.log(message)
        // console.log('webhookID -', webhookId)
        if (webhookId === WEBHOOK_ID) {
            for await (const emoji of REACTIONS) {
                await message.react(emoji.icon);
            }
        }
    }
};
