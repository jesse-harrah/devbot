/** ON MESSAGE */

module.exports = {
	name: 'messageReactionAdd',
    niceName: 'Applications Reactions',
	async execute(reaction, user, client) {
        console.log('beep boop', reaction)
        // console.log('test', reaction)
        if (reaction.message.author && reaction.message.author.bot) return;
        const APPLICATIONS_WEBHOOK_ID = '825412469735948348';

        const APPLICATIONS_CHANNEL_ID = '825412645502189618';
        const DENIED_CHANNEL_ID = '896767554561917000';
        const ACCEPTED_CHANNEL_ID = '896767896154423366';
    
        //%F0%9F%9F%A2 // %F0%9F%9F%A0 //%F0%9F%94%B4
        const EMOJI_GREEN_CIRCLE = '%F0%9F%9F%A2';
        const EMOJI_ORANGE_CIRCLE = '%F0%9F%9F%A0';
        const EMOJI_RED_CIRCLE = '%F0%9F%94%B4';
    
        const EMOJI_X = '%E2%9D%8C';
        const EMOJI_CHECK = '%E2%9C%85';
    
        const REQUIRED_COUNT = 3; // How many ppl are required to react (add 1 due to bot reaction)
    
        if (reaction.partial) {
            try {
                await reaction.fetch();
            } catch (error) {
                console.error('Something went wrong when fetching the message:', error);
                return;
            }
        }
    
        const { message } = reaction;
        // const message = reaction;
        console.log('test', {
            channelId: message.channelId,
            appId: APPLICATIONS_CHANNEL_ID
        })
        if (message.channelId !== APPLICATIONS_CHANNEL_ID || !message.webhookId || message.webhookId !== APPLICATIONS_WEBHOOK_ID) return console.log('MESSAGE NOT PART OF APPLICATION WEBHOOK');
    
        console.log(reaction);
        const content = message.content.toString();
        const embed = message.embeds[0] || null;
        const applicantName = embed.fields[1].value || '';

    
        const { identifier, name } = reaction.emoji;
    
        let accepted = false;
        let processed = false;
    
        console.log('identifier', identifier);
        console.log('test', {
            identifier,
            REQUIRED_COUNT,
            count: reaction.count
        })
        switch(true) {
            case identifier === EMOJI_CHECK: // Move application to accepted.
            case identifier === EMOJI_GREEN_CIRCLE && reaction.count >= REQUIRED_COUNT:
                console.log('entering accepted')
                accepted = true;
                client.channels.cache.get(ACCEPTED_CHANNEL_ID).send({ 
                    content,
                    embeds: message.embeds, 
                });
                await message.delete();
                console.log('APPLICATION DELETED');
                processed = true;
                break;        
            case identifier === EMOJI_X: // Move application to denied.
            case identifier === EMOJI_RED_CIRCLE && reaction.count >= REQUIRED_COUNT:
                console.log('entering deleted')
                client.channels.cache.get(DENIED_CHANNEL_ID).send({ 
                    content,
                    embeds: message.embeds, 
                });
                await message.delete();
                console.log('APPLICATION DELETED');
                processed = true;
                break;
            case identifier === EMOJI_ORANGE_CIRCLE && reaction.count >= REQUIRED_COUNT:
                
                break;        
            default: 
                console.log('identifier', identifier);
        }
                        
        processed && message.channel.send({
            embeds: [{
                color: accepted ? '#6AA84F' : '#D92323', 
                title: `Application ${ accepted ? 'Accepted' : 'Denied' }  -  ${name}`,
                description: `${applicantName}'s application has been ${ accepted ? 'accepted' : 'denied' }.`,
            }]
        });
    
	},
};