const { prefix } = require('../config.json');
const Discord = require('discord.js');
const { guildId } = require('../config');
const utils = require('../utils');

/** COMMAND HANDLING */
module.exports = {
	name: 'messageCreate',
    niceName: 'Commands',
	async execute(message, client) {
        // console.log('message:', message);
		if (!message.content.startsWith(prefix) || message.author.bot || message.partial) return;

        const args = message.content.slice(prefix.length).trim().split(/ +/);
        const commandName = args.shift().toLowerCase();

        if (!client.commands.has(commandName)) return;
        const command = client.commands.get(commandName);

        // Handle cooldowns
        const { cooldowns } = client;
        if (!cooldowns.has(command.name)) cooldowns.set(command.name, new Discord.Collection());

        const now = Date.now();
        const timestamps = cooldowns.get(command.name);
        const cooldownAmount = (command.cooldown || 0) * 1000;

        if (cooldownAmount && timestamps.has(message.author.id)) {
            const expirationTime = timestamps.get(message.author.id) + cooldownAmount;
            if (now < expirationTime) {
                const timeLeft = (expirationTime - now) / 1000;
                return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
            }
        }

        timestamps.set(message.author.id, now);
        cooldownAmount && setTimeout(_ => timestamps.delete(message.author.id), cooldownAmount);

        try {
            if (command.permissions) {
                const authorPerms = message.channel.permissionsFor(message.author);
                if (!authorPerms || !authorPerms.has(command.permissions))  return message.reply('You can not do this!');
            }

            if (command.args && !args.length) {
                let reply = `You didn't provide any arguments, ${message.author}!`;
        
                if (command.usage) reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
                
                return message.channel.send(reply);
            }
            if (command.tagUser && !message.mentions.users.size) return message.channel.send(`You must at least message 1 user.`);

            command.execute(message, args, client, commandName);
            if (command.prune) {
                message.delete({ timeout: 2500 });
                console.log('message pruned');
            }
            utils.sendLog(
                client, 
                message, 
                '',
                'orange'
            );
        } catch (error) {
            console.error(error);
            message.reply('there was an error trying to execute that command!');
        }
	},
};
