
const { MessageButton, MessageActionRow } = require('discord-buttons'); // To require only certain extensions that actually do the work!
const Discord = require('discord.js');

function createEmbed(opts) {
    return new Discord.MessageEmbed()
        .setColor(opts.color)
        .setTitle(opts.title)
        .setURL(opts.url)
        .setAuthor(...opts.authors)
        .setDescription(opts.description)
        .setThumbnail(opts.thumbnail)
        .addFields(...opts.fields)
        .setImage(opts.image)
        .setTimestamp()
        .setFooter(...opts.footer);
}

function createButton(name, url) {
    return new MessageButton().setStyle('url').setLabel(name).setURL(url);
}

module.exports = {
	name: 'buttons',
    aliases: [ 'links' ],
    dontLoad: true,
    prune: true,
	description: 'A useful list of buttons and links.',
    execute(message) {
        console.log('buttons');
        // ROW 1 | BCRP LINKS
        let queens = createButton('Queens', 'https://reee.me/queens');
        let brooklyn = createButton('Brooklyn', 'https://reee.me/brooklyn');

        // ROW 2 | SINCLAIR T&R LINKS
        let timeSheet = createButton('Timesheet', 'https://docs.google.com/spreadsheets/d/1xQ5miB2CMB-4TlLzLpaEJ0rY-YpIgkLzvLRp2IUaZSc/edit#gid=844948593');
        let applications = createButton('Applications', 'https://docs.google.com/spreadsheets/d/1sYDJRjZqmSHs_l4p2xgOjJ3pEUAwG363C3tieSTOxzw/edit#gid=1119778140');
        let timeTrials = createButton('Time Trials', 'https://docs.google.com/spreadsheets/d/14JgExNJJch2Hpe3Yx1ok3X7hjty6G7KXU6DcMkBDtSY/edit#gid=0');

        // ROW 3 | 
        let neighborhoodMap = createButton('Neighborhood Map', 'https://images-ext-2.discordapp.net/external/wAxCEpmHJtM5jkHC0P8Xsg6lb4DmF_jm8Fq2CZLf5tE/https/i.redd.it/5cw11krz9kcz.jpg?width=676&height=676')
        let jobApplication = createButton('Job Application', 'https://forms.gle/wiVba1RENvh9v1sg9');
        let shop4MLOTodo = createButton('Shop 4 MLO TODO', 'https://hackmd.io/Q_53LURrSc66mVkx0c8doQ?both#Shop-4-Exterior2');

        // ROW 4 |
        let shop4SOP = createButton('Shop 4 SOP', 'https://docs.google.com/document/d/1YkjyiiZugot2wHHRg2oi9jeceG3D39awsIQlpbWMDkg/edit?usp=sharing');
        let bcrpMechSOP = createButton('BCRP Mech SOP', 'https://docs.google.com/document/d/1zIa9wi2GM_eIkvvOfuD2vM6saCE-1KNNW-ryaIvayr0/edit?usp=sharing');

        let rows = [
            new MessageActionRow()
                .addComponents([ queens, brooklyn ]),
            new MessageActionRow()
                .addComponents([ timeSheet, applications, timeTrials ]),
            new MessageActionRow()
                .addComponents([ neighborhoodMap, jobApplication, shop4MLOTodo ]),
            new MessageActionRow()
                .addComponents([ shop4SOP, bcrpMechSOP ])
        ];

        message.channel.send('Useful Links', {
            // embed: createEmbed({
            //     color: '0099ff',
            //     title: 'Useful links',
            //     url: 'https://discord.js.org/',
            //     authors: [ 'Defiled Iron' ],
            //     description: 'Useful links for the interwebs.',
            //     thumbnail: 'https://i.imgur.com/wSTFkRM.png',
            //     fields: [
            //         { name: 'Regular field title', value: 'Some value here' },
            //         { name: '\u200B', value: '\u200B' }, // Empty line
            //         { name: 'Inline field title', value: '[Youtube](https://youtube.com/ \'optional hovertext\')', inline: true },

            //     ],
            //     image: 'https://i.imgur.com/wSTFkRM.png',
            //     timestamp: true,
            //     footer: [ 'Some footer text here', 'https://i.imgur.com/wSTFkRM.png' ]
            // }),
            components: rows
        });
        // message.channel.send('\w', { components: rows });
    }
};