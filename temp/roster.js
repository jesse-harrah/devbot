const { google } = require('googleapis');
const { shopName } = require('../config.json');
const { MessageEmbed } = require('discord.js');
const { MessageButton } = require('discord-buttons'); // To require only certain extensions that actually do the work!
const { spreadsheetId, sheetName } = require('../config.json');


module.exports = {
	name: 'roster',
    aliases: [ ],
	dontLoad: true,
	description: 'View the current roster.',
    usage: '',
    args: false,
    tagUser: false,
    cooldown: 5,
    execute(message, args, client) {
        const sheets = google.sheets('v4');
		// console.log('auth', client.google.jwtClient)
		sheets.spreadsheets.values.get({
			auth: client.google.jwtClient,
			spreadsheetId: spreadsheetId,
			range: sheetName
		}).then(async sheet => {
			const employees = [ ];
			for (let row of sheet.data.values) {
				let name = row[0];
				
				if (name === 'Name' || name === 'Mechs' || name === 'Tow' || name === 'Total') { console.log(`Skipping ${name} row.`); continue; };
				if (!name || name === ' ' || name === undefined) { console.log(`Skipping ${name} row. 2`); continue; }
				
                let title = row[1];
                const emp = { name, value: title, inline: false };
                employees.push(emp);
			}

			const embed = new MessageEmbed()
				.setTitle(shopName)
				.setColor('#278f42')
				.setDescription(`**Employee Roster**\n\n`)
				.setThumbnail(`https://i.gyazo.com/57dbe9e40c74555d7cf613e69bbc3580.png`)
                // .addField('\u200b', '\u200b')

            let curType;

			for (let i = 0; i < employees.length; i++) {
                if (i === 0) curType = employees[i].value;
				if (employees[i].value !== curType) {
                    embed.addField('\u200b', '\u200b');
                    curType = employees[i].value;
                }
                embed.addField(employees[i].name, employees[i].value, employees[i].inline);
                
			}

			embed
				.setTimestamp()
				.setFooter('Coded with ❤')
		

			message.channel.send({ 
				embed, 
				component: new MessageButton()
					.setStyle('url').setLabel('Shop Hours')
					.setURL('https://docs.google.com/spreadsheets/d/1xQ5miB2CMB-4TlLzLpaEJ0rY-YpIgkLzvLRp2IUaZSc/edit#gid=844948593') 
			});

			// message.channel.send(`**Employee Hours Week ${col}:** ${url}`);
				// .then(msg => {
				// 	message.delete({ timeout: 2500 });
				// });
		}).catch(error => {
            console.error('error', error);
            message.channel.send(`The API returned an error:\n\`\`\`\n${error}\n\`\`\``) 
        });
    }
};

// {
//     name: '\u200b',
//     value: '\u200b',
//     inline: false,
// },
