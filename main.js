const gKeys = require('./google-keys2.json');
const { clientId, guildId, token, firebaseKeys } = require('./config');
const { Client, Intents } = require('discord.js');

const { setupEvents, setupCommands } = require('./utils');
const { google } = require('googleapis');
const firebase = require('firebase');

let jwtClient = new google.auth.JWT(
    gKeys.client_email,
    null,
    gKeys.private_key,
    [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/youtube'
    ]);

//authenticate gapi request
jwtClient.authorize(function (err, tokens) {
    if (err) {
        console.log(err); return;
    } else { console.log("Successfully connected to google Api-s!"); }
});

const app = firebase.initializeApp(firebaseKeys);

const client = new Client({ 
    intents: [ Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.DIRECT_MESSAGES ],
    partials: [ 'MESSAGE', 'CHANNEL', 'REACTION']
});
setupEvents(client);
setupCommands(client);

// client
//   .on("debug", console.log)
//   .on("warn", console.log)

client.login(token).then(res => { 
    console.log('client logged in'); 
    client.google = {};
    client.google.jwtClient = jwtClient;
    client.google.auth = jwtClient;
}).catch(error => console.error('error', error));

function setGoogleToken(client, jwtToken) {
    client.google = {};
    client.google.jwtClient = jwtToken;
    client.google.jwtToken = jwtToken;
    client.google.auth = jwtToken;
}
