const POST_URL = "https://discord.com/api/webhooks/892333257616683008/mfnYXn7WkeIqUome-P4NicAAQgvlBcdxUiA3Zi6Y1_8BnMFiOYTS-auAzD4mn-vluVb7";

const TITLE = 'Burger Shot Applications';
function postAllApplications() {
  const form = FormApp.getActiveForm();
    const allResponses = form.getResponses();
    console.log('allResponses', allResponses);
    const latestResponse = allResponses[allResponses.length - 1];

    allResponses.forEach((response, i) => {
      
      if (i === 30 || i === 29 || i === 31) console.log('test', response);
      const items = getMessage(response.getItemResponses());
      const options = {
        "method": "post",
        "headers": {
            "Content-Type": "application/json",
        },
        "payload": JSON.stringify({
            "content": "‌", // This is not an empty string
            "embeds": [{
                "title": TITLE,
                "fields": items,
                "footer": {
                  "text": "React your opinions!"
                }
            }]
        })
      };

      UrlFetchApp.fetch(OSAS_URL, options);
    });
}
postAllApplications()

function onSubmit(e) {
    const form = FormApp.getActiveForm();
    const allResponses = form.getResponses();
    console.log('allResponses', allResponses);
    const latestResponse = allResponses[allResponses.length - 1];

    const response = latestResponse.getItemResponses();

    const items = getMessage(response);

    const options = {
        "method": "post",
        "headers": {
            "Content-Type": "application/json",
        },
        "payload": JSON.stringify({
            "content": "‌", // This is not an empty string
            "embeds": [{
                "title": TITLE,
                "fields": items,
                "footer": {
                  "text": "React your opinions!"
                }
            }]
        })
    };

    UrlFetchApp.fetch(POST_URL, options);
    UrlFetchApp.fetch(OSAS_URL, options);
};

function getMessage(response) {
  const items = [];
  for (let i = 0; i < response.length; i++) {
    const question = response[i].getItem().getTitle();
    const answer = response[i].getResponse();
    let parts = [];
    try {
        parts = answer.match(/[\s\S]{1,1024}/g) || [];
    } catch (e) {
        parts = answer;
    }

    if (answer == "") { continue; }
    for (let j = 0; j < parts.length; j++) {
        if (j == 0) {
            items.push({
                "name": question,
                "value": parts[j],
                "inline": false
            });
        } else {
            items.push({
                "name": question.concat(" (cont.)"),
                "value": parts[j],
                "inline": false
            });
        }
    }
  }
  // console.log('items', items);
  return items;
}