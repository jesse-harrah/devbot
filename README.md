# :hammer_and_wrench: Sinclair T&R MLO's :hammer_and_wrench: 

###### `Shop 4` `MLO` `Sinclair T&R` `Extension Shop` `YMAP` `3DSMax` `CodeWalker` `OpenIV` `YTYP` `YTD` `YMF` `YBN` `Props` `Vanilla` `WIP`

> Todolist & details for the shop 4 interior/exterior. [color=#ddc400 ]

---

- [x] **Comment Legend**
    > INFO
 
    > Tim [color=#3b75c6]

    > NAGINA [color=#008900]

    > LENNERT [color=#f00000]

    > NARDAH? [color=#000005]
---

## **Table of Contents**
[ToC]
 
## :memo: **TODO**

### **Shop 4 Interior**

- [x] Remove lifts/explosive tanks (**including collisions**) `Codewalker` `3DSMax` 
    > **"should be straight forth"**
- [x] Try and remove paint booth (**need to edit the details file also what should go here?**)
    - [x] Remove booth
    - [x] Remove Details from booth (*inside/outside*)
    > **"should be straight forth"**
- [ ] Props, props, props `CodeWalker`
    > Need ideas from Nagina for this space, should we keep the shelves/parts/junk around the walls (*i think we should but make a kickass break room*) the paint booth will be gone so there will be alot of open space to fill in that area. [color=#008900]

> Will take a bit to get up and going but should be pretty straight forward, just need to remove the collisions for the lifts the **RIGHT** way in 3DS Max [color=#3b75c6]

### **Shop 4 Exterior**

- [x] Remove junk around parking lot (*Move objects with LOD's below the map*)
- [x] Lighting (**still need to remove one LOD light**)`CodeWalker`
    - [x] Move Vanilla shop light
    - [ ] Remove Vanilla shop light LOD `CodeWalker` `OpenIV`
    - [ ] Possibly add new shop light LOD???? `Unknown`
- [ ] Remove the Hayes Autos texture from outer brick wall `3DSMax`
- [ ] Exterior sign textures? `3DSMax`
    > **Need ideas for this from Nagina.** [color=#008900]
- [x] Remove car gen in front of ext-shop `CodeWalker`
- [x] Remove plywood and sheetmetal from extension shop building
- [ ] Props, props, props `CodeWalker`
    > **Have some ideas for props in this area but still need more.** [color=#008900]


> **This is pretty much finished, will have to remove the junk and cut out all the doors/windows, when/if all the interiors are done. LOD Light shouldn't be hard to delete and just need to prop the area up.** [color=#3b75c6]

### **Extension Shop** 

- [x] ==Re-texture waiting area ceiling== (*Need to figure out how to re-texture with textures from the model because im too lazy to look for and download textures*) `3DSMax` **Just used main area ceiling texture, can be retextured later just like the shop floor should be**)
- [ ] Find better shop floor texture. (**Maybe the one in the current shop?**)
    > **Nagina? ^** [color=#008900]
- [x] Remove oil pit and raise ceiling `3DSMax`
- [ ] ==Windows== (*pretty sure this will have to be done in 3DSMax with a texture*) `3DSMax` `CodeWalker?` 
    > ***Need to find good textures** (looking for something a little "grungy"/"dirty"/"frosted") [color=#3b75c6]

    > **Maybe Nagina? ^** [color=#008900]
- [ ] Turn storage area into like working area with work benches (*bennys*)
- [ ] ==Interior lighting sorted== `CodeWalker` (*Need to learn how to do this in `3DSMax` cause `CodeWalker` is not great for lighting*)
    > **Will take some research, I have no clue how to setup lighting in a 3D model.** [color=#3b75c6]
- [ ] ==Detail texture changed and adjusted to properly line up to the MLO== (or just bridge the shell so there are no holes *wont work on the garge doors part tho*) `3DSMax`
    > **Garage door threshold has a gap between the parking lot, trim around doors and windows have gaps between walls, trim around door between shop and waiting area is more thin than the rest.** [color=#3b75c6]
- [ ] ==Rotate man door 180 and have it hinge from the west.==
- [ ] ==Resize garage door, rails and exterior wall for taller doors.== `3DSMax` 
    > **Will need to reach out to Lennert, I have no clue how he did that shit** [color=#3b75c6]
- [ ] Need to find darker brick or other texture for detail model. (*stands out too much*)
    > Nagina ^ [color=#008900]
- [ ] Find out why objects in waiting room ar dark at night, most likely due to improper lighting.
    > **Nagina wants a special fixture in here so need to import that to the lights interior file and setup a proper light cone for it** [color=#008900]
- [ ] Add mural(s)/posters to interior (*high detail .ymap*) `3DSMax`
- [ ] Maybe add lift back after ceiling is raised `3DSMax` 
    > **Need confirmation from Nagina** [color=#008900]
- [ ] Scale bus and car, remove snake and create one model with all 3 `3DSMax` `CodeWalker` `WIP`
    > **Lot harder than it sounds**, but WIP [color=#3b75c6]
- [ ] Audio for garage doors and man door? 
    > **Ask Lennert**  [color=#f00000]
- [ ] Props, props, props `CodeWalker`
    > **Have lots of ideas but could use your help** [color=#008900]
- [ ] Interior and Props dissapear when going into 1st person while inside.... (**Need to figure this one out**)
    > **Ask Lennert** [color=#f00000]
- [ ] Find out why Casino DLC props dont render in MP

> TBF this interior is not far off, I think we should get this one and the exterior/maybe paintbooth, in the city first just to make sure everything is working correctly and is optimized with multiple people on a server [color=#3b75c6]

### **Paint Booth**

- [ ] Remove junk from the door `CodeWalker`
- [ ] Remove *fake* wall `3DSMax` `CodeWalker`
- [ ] Door? (*possibly scale up the actual paint booth door*) `CodeWalker` `3DSMax?`
    > **Do you want a garage door here? (*Adds alot of complexity*) Will do try it if you want tho** [color=#008900]

    > **IF SO, Tim ask Lennert**  [color=#f00000]
- [ ] Find Textures for floor/walls/ceiling - [todo](#) `CodeWalker` `3DSMax`
    - Floor: **todo**
    - Wall: **todo**
    - Ceiling: **todo**
    :::info 
    :bulb: Try to grab the texture and detail for a paintbooth for all 3 above textures. (*Room has the potential to be quite large... may be good to have a second room or just extend the Employee area into it.)
    :::
    > **Nagina ^ (*I can grab the paint booth textures but I need to know what to do with the rest of the building*)** [color=#008900]
- [ ] Create Interior/Collision Model `3DSMax` `CodeWalker`
- [ ] Create Details Model `3DSMax` `CodeWalker`
- [ ] Create Lighting `3DSMax` `CodeWalker`
    > **Ask Lennert** [color=#f00000]
- [ ] Props, props, props `CodeWalker`
    > **Shouldn't really need much here, (lights, decals, air compressor, spray gun, maybe a table with some paint cans?)** [color=#3b75c6]

> **Should be fairly straightforward, box with a large hole, collision, lights, textures, props, done....** [color=#3b75c6]

### **Employee Area (Hideout)**

    - Entry
    - Bathroom
    - Lounge/Break Area
        - Storage Box
        - Pool Table
    - Office
    - Lockeroom
    - Small Sleeping Area if possible

- [ ] Remove door collission `3DSMax`
- [ ] Add door (*texture?*) `3DSMax?` `CodeWalker`
- [ ] Add windows (*texture?*) `3DSMax?` `CodeWalker`
    > **Find out from Lennert how he did the doors and windows in the current interior, they seem to be embedded...** [color=#f00000]
- [ ] Find Textures for floor/walls/ceiling
    - Floor: **todo**
    - Walls: **todo**
    - Ceiling: **todo**
    > **Nagina ^ (*We need textures for the above 3 things for all the rooms listed above*)** [color=#008900]
- [ ] Create Interior/Collision Model `3DSMax`
- [ ] **IMPORTANT** - ==Create seperate rooms and get coordinates for portals== `3DSMax` `CodeWalker`
- [ ] Map *any* stairs
    > **EASY, just select the 'Map Stairs' checkbox on the collision children mesh modifier** [color=#3b75c6]
- [ ] Create Details Model `3DSMax` `CodeWalker`
- [ ] Create Lighting `3DSMax` `CodeWalker`
    > **Ask Lennert** [color=#f00000]
- [ ] Props, props, props `CodeWalker`
    > **100% you :kissing_heart: have fun designing this area** [color=#008900]

> **Fairly complex, a combination of everything else learned above, plus the addition of rooms. Will wanna take time with this one to do it right. Its a long narrow place, 2 occlusions, 4 rectangle windows and a man door. Could potentially spill into paintbooth building.** [color=#3b75c6]

> **Need to ask Lennert how to setup rooms (*assuming 3DS max and just mapping portals*)** [color=#f00000]
### **General**

- [ ] ==Weld all vertex's==
- [ ] Scripts for ATM/VENDING/LIFT/etc.
- [ ] `Shop 4` car garage prop (**Need prop file** - Nardah maybe?)
    > **RIGHT...** [color=#000005]
- [ ] `Shop 4` buisness circle prop (**Need prop file** - Nardah maybe?)
    > **RIGHT...** [color=#000005]
- [ ] Create `YTD` texture dictionaries (*Better performance*)
- [ ] *Possibly* fix other entrance sliding door????
    > Think i just need to copy the working door and move the pivot.

## :couch_and_lamp: **Props** 
 
> ==:bulb: List of props being used or potential props to use.==

### **Vanilla ymap**
- prop_jyard_block_01a
- prop_wall_light_15a

### **Shop 4 Exterior**
- prop_portaloo_01a
- prop_byard_gastank01
- prop_byard_gastank02
- prop_recyclebin_05_a
- prop_venice_sign_02
- prop_flattruck_01a
- prop_plate_stand_01
- prop_sacktruck_01
- prop_picnictable_01
- v_serv_waste_bin1
- prop_elecbox_10
- prop_fire_exting_2a
- prop_flattruck_01b
- v_ind_cfemlight

### **Extension Shop**

- imp_prop_impexp_mechbench
- prop_rub_boxpile_08
- prop_cctv_cam_07a
- v_res_tre_alarmbox
- prop_wall_light_01a
- prop_elecbox_21
- v_ind_cfemlight
- v_ind_cs_striplight
- prop_bucket_01a
- prop_buckets_02
- v_ind_cm_tyre03
- prop_barrel_02a
- prop_carjack
- prop_oilcan_01a
- prop_jerrycan_01a
- v_ind_cs_gascanister
- v_ind_cs_paint
- v_ind_cs_spray
- v_ind_cs_toolbox3
- v_ind_cs_toolbox2
- prop_toolchest_03
- prop_oilcan_02a
- prop_toolchest_04
- v_ind_cs_axe
- v_ind_cs_tray04
- prop_toolchest_02
- v_ind_cs_wrench
- v_ind_cs_mallet
- v_ind_cs_drill
- v_ind_cs_pliers
- v_ind_cs_spanner04
- prop_battery_01
- v_ind_cs_oilbot05
- v_ind_cs_toolbox1
- v_ind_cs_jerrycan01
- prop_cardbordbox_05a
- v_ind_cs_paper
- v_ind_cs_tray01
- v_ind_cs_powersaw
- v_ind_cs_tray04
- v_ind_cs_tray03
- prop_compressor_01
- prop_compressor_03
- prop_engine_hoist
- prop_carcreeper

### Props from bunker

- prop_gascage01
- gr_prop_gr_prop_welder_01a
- gr_prop_gr_tool_draw_01b
- gr_heavy_door
- v_ind_rc_locker
- v_ind_rc_lockeropn
- v_ind_meatbench
- gr_prop_gr_tool_box_02a
- gr_prop_gr_tool_box_01a
- gr_prop_gr_grinder_01a
- v_ind_rc_workbag
- gr_prop_gr_tool_draw_01d
- v_ind_rc_lowtable
- v_ret_fh_ashtray
- v_club_vu_coffeecup
- prop_cctv_cam_05a
- prop_watercooler
- v_serv_tc_bin2_
- v_serv_tc_bin2_
- prop_vend_coffe_01
- gr_bunker_plan_board
- v_ind_dc_desk01
- v_ind_dc_desk02
- prop_paper_box_02
- prop_printer_01
- v_med_storage
- prop_printer_01
- v_ind_cm_light_on
- v_corp_divide
- imp_prop_impexp_half_cut_rack_01a
- **imp_prop_wheel_balancer_01a**
- prop_ind_mech_03a (wall pump)
- imp_prop_tool_draw_01c
- prop_car_battery_01
- v_ind_cm_crowbar
- imp_prop_car_jack_01a (red jack dwn)
- **imp_prop_axel_stand_01a**
- prop_tyre_rack_01
- v_ind_cs_toolbox4
- imp_prop_impexp_car_door_02a
- imp_carwarecar_ware_cardoors

> What am i doing with my life?
## :frame_with_picture:  **Textures & Ideas**

> **Mural Ideas**
![Mural][Mural-1]

---

> **Poster Ideas**
![Poster][Poster-1]
![Poster][Poster-2]
![Poster][Poster-3]

---

### **Shop 4 Interior**


### **Shop 4 Exterior**

> Some kind of mural on the brick wall would be dope

### **Extension Shop**


### **Paint Booth**

> *Won't need alot extra here other than native textures unless we open it up more*

### **Employee Area (Hideout)**

> *Will need Floor/Ceiling/Wall textures for all the rooms in this MLO

---

## :open_file_folder: **Files**

### Main

- `_hayesauot.ymf`

### **Shop 4 Interior**

- 

### **Shop 4 Exterior**

- `di_hayes_exterior.ymap` `YMAP`
- `di_hayes_exterior_lights.ymap` `YMAP` 
- `lr_sc1_06_strm_0.ymap` `Vanilla` `YMAP`
- 

### **Extension Shop**

- `nm_hayesauto.ytyp` `YTYP`
- `nm_hayesauto_col.ybn` `YBN`

### **Paint Booth**

-

### **Employee Area (Hideout)**

-

## **File Types**

- YMF - Manifest File
    - Main manifest XML file, tells GTA which files to overwrite and which `Props` each file is using.

- YMAP - Map files
    > Interiors need the props attatched to their YTYP to show up while inside.
    - Exterior: Handles props/entities/car-gens
    - Interior: Handles collisions

- YTYP
    - Handles `Props`/Archetypes for interiors

- YBN
    > ==:bulb: Some props will need to have collision created for the `YTYP`/`YMAP` you're adding them to.==
    - Collision files
    - Created when you create your `YTYP`/`YMAP` in `3DSMax`
     
## **Notes for Tim**

> **`LOD`** - Annoying/BiggestPainInTheAss things ever.
    > - Used by GTA to render a *playdough* version of objects from far away. (*Same reason you can see the objects that used to be at `Shop 4` from a distance* - I have moved them from my map)
    > - **DO NOT DELETE A `Props` `LOD`** (*Just move the `Props` and `LOD`s under the map or far away.*)
    > - There are Light LOD's & Object LOD's
    > - Never delete an object that has an LOD (*Headaches insue and you start over*)
    > - Light LOD's need to be found in the LOD `YMAP` via X & Y coordinates of the light and removed (*Otherwise you will have floating light particles where the light used to be*)


> ==Always save calculate extents and save any `YMAP` you are editing even if it doesnt show its been changed== (*Causes props/collisions to be in weird places in game*)


> **Never add props to Vanilla `YMAP`s** 
    > - You can delete props **if they dont have a LOD**
    > - Create a new YMAP and add the props directly to it (**UPDATE MANIFEST**)



## Tooling

`CodeWalker` - Used for finding/moving/adding objects to ymap's and ytyp files, also creates your manifest file.

`OpenIV` - Used for traversing GTA files and rpf files.

`3DSMax` - Used for manipulating/creating game object and collision files.


## :link: **Links/Docs/Tutorials**

| Name              | Description             | Link                    |
| ----------------- | ----------------------- |:----------------------- |
| Prop Browser      | Browse *most* GTA props | [:link:][Prop-Browser]  |
| Scaling Props 1   | Scaling props Wrong Way!|[:link:][Scaling-Props-1]|
| Scaling Props 2   | Scaling props help      |[:link:][Scaling-Props-2]|
| Dekurwinator      | Lots of good tutorials  | [:link:][Dekurwinator]  |


| Docs              | Description             | Link                    |
| ----------------- | ----------------------- |:----------------------- |
| Instanced Interior| Instanced Interior      |[:link:][Instanced-Interior]|
|FiveM Scripting Docs| FiveM module links     |[:link:][Fivem-Docs]     |


| Tuts              | Description             | Link                    |
| ----------------- | ----------------------- |:----------------------- |
| CodeWalker YT     | YT Tutorial             | [:link:][Codewalker-YT] |
| Working Stairs    | YT Tutorial             | [:link:][Working-Stairs]|
|Remove Vanilla Lights| Remove Light LOD's    | [:link:][Remove-Vanilla-Lights]|


| Vids              | Description             | Link                    |
| ----------------- | ----------------------- |:----------------------- |
| Interior Fix 1    | Blinking fix            | [:link:][Interior-Fix-1]|


| Useful Mods       | Description             | Link                    |
| ----------------- | ----------------------- |:----------------------- |
| vSync             | Time/Weather Commands   | [:link:][vSync]         |


<!-- LINKS -->
[Prop-Browser]: https://mwojtasik.dev/tools/gtav/objects
[Codewalker-YT]: https://www.youtube.com/channel/UCvig3BG9Bje3--CbcX3FvJQ
[Scaling-Props-1]: https://forum.cfx.re/t/ymap-spawning-bigger-props-and-ytyp/183927
[Scaling-Props-2]: https://forums.gta5-mods.com/topic/22125/help-codewalker-objects-not-scalling-in-game/9
[Working-Stairs]: https://www.youtube.com/watch?v=rDqSnyGO9_I
[Dekurwinator]: https://dekurwinator-mods.bitrix24.site/tutorials/

<!-- Docs -->
[Instanced-Interior]: https://gta-fivem.eu/how-to-map-an-interior-and-stream-it-in-fivem/
[Fivem-Docs]: #

<!-- Tutorials -->


[Remove-Vanilla-Lights]: #


<!-- YT Vids -->
[Interior-Fix-1]: https://www.youtube.com/watch?v=7_lKCFi8I40&ab_channel=Lafa2K

<!-- Pictures -->
[Mural-1]: https://cdn.discordapp.com/attachments/794937323443453982/849547084990119957/b7c248c885c1133fc3b03066aa1b6639.png

[Poster-1]: https://cdn.discordapp.com/attachments/794937323443453982/849547440050012190/Service-Station-Logo-Wall-Decal-Garage-Sign-Wall-Art-Sticker-Mechanic-Gifts-Decoration-Car-Service-W.png
[Poster-2]: https://cdn.discordapp.com/attachments/794937323443453982/849547529698803748/il_340x270.png
[Poster-3]: https://cdn.discordapp.com/attachments/794937323443453982/849547706938294282/Mechanical-Heart-Engine-Repair-Wall-Art-Sticker-Decal-For-Garage-Shop-Decoration-Removable-A002140.png

<!-- Useful Mods -->
[vSync]: https://forum.cfx.re/t/vsync-v1-4-0-simple-weather-and-time-sync/49710


---
Coded with :heart: